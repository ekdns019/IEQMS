<!doctype html>
<html lang="ko">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no" />
    <title><?=$TITLE?></title>

    <!-- Layout -->
    <link href="<?=CSS.$this->__SEG[1]?>/style_common.css" rel="stylesheet" type="text/css">

    <?
    switch ($this->__SEG[1]){

        case "total" :
            break ;


        default :
            break ;
    }
    ?>
<!--    <link href="--><?//=CSS.$this->__SEG[1]?><!--/style_contents.css" rel="stylesheet" type="text/css">-->
<!--    <link href="--><?//=CSS.$this->__SEG[1]?><!--/style_total.css" rel="stylesheet" type="text/css">-->
    <!-- /Layout -->

    <!-------------- 공통 -------------->
    <!-- font(본고딕) -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900&display=swap" rel="stylesheet">
    <!-- /font(본고딕) -->
    <link href="<?=CSS?>common/reset.css" rel="stylesheet" type="text/css">
    <!-- Icon -->
    <link href="<?=CSS?>common/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?=CSS?>common/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- /Icon -->
    <!-- 상단 드롭다운 메뉴 -->
    <link href="<?=CSS?>common/profile_dropdown.css" rel="stylesheet" type="text/css">
    <!-- /상단 드롭다운 메뉴 -->
    <!-- Switch체크박스 -->
    <link href="<?=CSS?>common/components.css" rel="stylesheet" type="text/css">
    <!-- /Switch체크박스 -->
    <!-- 패널 Hidden/Show -->
    <link href="<?=CSS?>common/core.css" rel="stylesheet" type="text/css">
    <!-- /패널 Hidden/Show -->
    <!-- 달력 -->
    <link href="<?=CSS?>common/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- /달력 -->

    <!-- 상단 드롭다운 메뉴 -->
    <script type="text/javascript" src="<?=JS?>common/jquery.min.js"></script>
    <script type="text/javascript" src="<?=JS?>common/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=JS?>common/blockui.min.js"></script>
    <!-- /상단 드롭다운 메뉴 -->

    <!-- 좌측 사이드바 -->
    <script type="text/javascript" src="<?=JS?>common/app.js"></script>
    <!-- /좌측 사이드바 -->
</head>
<body>
<iframe id="actionFrm" name="actionFrm" style="display: none;"></iframe>
<div class="wrap">
