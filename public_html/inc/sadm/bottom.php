<!-- 달력 -->
<script type="text/javascript" src="<?=JS?>common/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="<?=JS?>common/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="<?=JS?>common/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript" src="<?=JS?>common/plugins/pickers/pickadate/picker.date.js"></script>
<!-- /달력 -->

<!-- 좌측영역 Toggle -->
<script type="text/javascript">
    $(document).ready(function () {
        var trigger = $('.hamburger'),
            overlay = $('.overlay'),
            isClosed = true;

        trigger.click(function () {
            hamburger_cross();
        });

        function hamburger_cross() {

            if (isClosed == true) {
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
            } else {
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function () {
            $('.toggle_wrap').toggleClass('toggled');
        });
    });

</script>
<!-- /좌측영역 Toggle -->

<!-- GNB 영역 -->
<script>
    $('#nav li').bind('mouseenter keyup', function() { // 메뉴바의 각 메뉴들에 마우스를 올리거나 키보드로 이동하면,
        $(this).addClass('on').siblings().removeClass(); // 해당 메뉴에 클래스 on을 추가하고, 다른 메뉴의 클래스를 제거합니다.
    });
</script>
<!-- / GNB 영역 -->

</div>
</div>
</body>
</html>
