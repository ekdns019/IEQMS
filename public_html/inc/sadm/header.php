<div class="header">
    <div class="logo">
        <img src="<?=IMAGE?>common/toplogo.png">
    </div>
    <div id="gnb">


        <div id="nav">
            <?
            echo print_r($MENU) ;
            ?>
            <ul>
                <li>
                    <a href="javascript:void(0)">시험관리</a>
                </li>
                <li>
                    <a href="javascript:void(0)">D-TEST</a>
                </li>
                <li>
                    <a href="../../sadm/contents/q_list.html">컨텐츠 관리</a>
                    <ul class="snb smenu3">
                        <li>
                            <a href="../../sadm/contents/exam.html">내신 기출 시험지</a>
                        </li>
                        <li>
                            <a href="../../sadm/contents/pset.html">모의 · 수능 · 사관 기출 및 PSET 시험지</a>
                        </li>
                        <li>
                            <a href="../../sadm/contents/concept.html">개념관리</a>
                        </li>
                        <li>
                            <a href="../../sadm/contents/type.html">단원 및 유형</a>
                        </li>
                        <li>
                            <a href="../../sadm/contents/category.html">분류 관리</a>
                        </li>
                        <li>
                            <a href="../../sadm/contents/qr.html">QR코드 관리</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)">고객지원 관리</a>
                </li>
                <li class="on">
                    <a href="javascript:void(0)">통합관리</a>
                    <ul class="snb smenu5">
                        <li>
                            <a href="../../sadm/total/member.html">회원관리</a>
                        </li>
                        <li>
                            <a href="../../sadm/total/setup.html">권한관리</a>
                        </li>
                        <li>
                            <a href="../../sadm/total/staff.html">직급관리</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>


    </div>



    <div class="top_r">
        <!-- 우측 프로필 -->
        <div class="top_profile">
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <!-- 담당자 정보 -->
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?=IMAGE?>common/placeholder.jpg" class="img-circle img-sm" alt="">
                            <span class="user_name"><?=$this->admInfo['name']?></span>
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0);" data-logout-btn="">
                                    <i class="icon-switch2"></i> Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- //담당자 정보 -->
                </ul>
            </div>
        </div>
        <!-- //우측 프로필 -->
    </div>
</div>


<div class="total-top"></div>

<div class="page-container">
