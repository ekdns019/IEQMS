<html lang="ko">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PS EDU</title>
        <link rel=" shortcut icon" href="/favicon.ico">
        <link rel="icon" href="/favicon.ico">

        <!-- Layout -->
        <link href="<?=CSS?>sadm/style_common.css" rel="stylesheet" type="text/css">
        <link href="<?=CSS?>sadm/style_contents.css" rel="stylesheet" type="text/css">
        <link href="<?=CSS?>sadm/style_login.css" rel="stylesheet" type="text/css">
        <link href="<?=CSS?>sadm/style_total.css" rel="stylesheet" type="text/css">
        <!-- /Layout -->

        <!-------------- 공통 -------------->
        <!-- font(본고딕) -->
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900&display=swap" rel="stylesheet">
        <!-- /font(본고딕) -->
        <link href="<?=CSS?>common/reset.css" rel="stylesheet" type="text/css">
        <!-- Icon -->
        <link href="<?=CSS?>common/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?=CSS?>common/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <!-- /Icon -->
    </head>
    <body>

        <iframe id="actionFrm" name="actionFrm" style="display:none;"></iframe>

        <!-- 전체레이아웃 -->
        <div class="login_wrap">
            <div class="login_container">
                <div class="login_l">
                    <div class="login_l_inner">
                        <div class="login_logo">
                            <img src="<?=IMAGE?>sadm/login/login_logo.png">
                        </div>
                        <p class="login_txt1">EXAM BANK SYSTEM</p>
                        <p class="login_txt2">문제은행관리시스템</p>
                    </div>
                </div>
                <div class="login_c"></div>
                <div class="login_r">
                    <div class="login_r_inner">
                        <form name="frm" method="post" action="login/login" role="form" data-toggle="validator" target="actionFrm">
                            <div class="login_r_tt">
                                <img src="<?=IMAGE?>sadm/login/login_tt_img.png">
                            </div>
                            <div class="login_r_input">
                                <div class="r_inp_id">
                                    <ul>
                                        <li class="inp_id_ico">
                                            <img src="<?=IMAGE?>sadm/login/login_id_picto.png">
                                        </li>
                                        <li class="inp_id_box">
                                            <input type="text" name="userid" id="userid" value="" placeholder="ID (아이디)" maxlength="20" required>
                                        </li>
                                    </ul>
                                </div>
                                <div class="r_inp_id">
                                    <ul>
                                        <li class="inp_id_pw">
                                            <img src="<?=IMAGE?>sadm/login/login_pw_picto.png">
                                        </li>
                                        <li class="inp_id_box">
                                            <input type="password" name="userpwd" id="userpwd" value="" placeholder="PW (비밀번호)" maxlength="20" required>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="login_r_btn">
                                <button type="submit" class="loing_btn">Log in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- //전체레이아웃 -->

    </body>
</html>
