<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


// 다운 지정상수
define('Du', 'config/trait')          ; // trait 경로
define('CSS', '/css/')                ; // css 경로
define('JS', '/js/')                  ; // js 경로
define('VENDOR', '/vendor/')          ; // VENDOR 경로
define('IMAGE', '/images/')           ; // image 경로
define('FONT', '/font-awesome/')      ; // font awesome경로
define('COMMON', 'SC')                ; // 사이트 db공통코드
define('BANK', '!@#BANK$%^')           ; // 암호화 code
define('direct_omr', 'upload_exam')    ; // exam omr 시험 임시저장폴더
define('curriculum_upload', $_SERVER["DOCUMENT_ROOT"].'/upload_curriculum/')    ; // 커리큘럼 PDF 저장폴더
define("TOKEN_URI","http://bank.ps-e.co.kr/oauth/auth/accessToken") ;
define("TOKEN_AUTH","http://bank.ps-e.co.kr/oauth/auth/response") ;



define('CODE', 'du123455!@#')         ; // 암호화 code
define('MDB','TOPTIER')               ;

define('cstSurl','d66c49aa9b30a6f2066f62c169f11d16');
define('cstNaver_ClientID','yg3k7UReCcv6bygPp2X_');
define('cstNaver_Secret','aEIGI_cAIF');

define('cstEScode','ceg_1605499586000');	//초등학교 코드
define('cstMScode','ceg_1605499596000');	//중학교 코드
define('cstHScode','ceg_1605499616000');	//고등학교 코드

define('cstCeacode','cea_1605499923000');	//시험분류 입학 코드


//4142229
define('vimeo_folder','4142229') ;
define('vimeo_user_num','137695051') ;
define('vimeo_c_id','401aa254acdcf0c7ed02c22e91c79e75b46035f6') ;
define('vimeo_cs_id','vEOlx47jxE3afMqjavnYqQlbZJqJAjPENVjqr4vwTMnQDXTUQu4m5+xNv+E+tOiIOKIGx3GhdOIwMfTnanGM6JV3XfUBAmbqtC/uyScQd6tLEnykgTj21o3uvZABAuqh') ;
define('vimeo_token','390fd8c0a1f8616612a87f9d253416bf') ;
