<?php

defined('BASEPATH') OR exit('No direct script access allowed') ;
trait Trait_auth{
    public function USER_AUTH($id){
        if(isset($this->session->userdata[$id])){
            return true ;
        }else{
            return false ;
        }
    }

    private function getClient($id,$type){

        $_databases = $type == "_ADMIN" ? "IEQMS_ADM" : "IEQMS_USR" ;
        $_tables    = $type == "_ADMIN" ? "TBL_ADM"   : "IEQMS_USR" ;
        $_field     = $type == "_ADMIN" ? "adm_id"    : "IEQMS_USR" ;
        $_name      = $type == "_ADMIN" ? "adm_name"  : "IEQMS_USR" ;
        $_code      = $type == "_ADMIN" ? "adm_code"  : "IEQMS_USR" ;

        $_sql = " SELECT
                    t1.{$_field} as user_id      ,
                    t1.{$_name}  as user_name    ,
                    t1.{$_code} as user_code     ,
                    t2.client_id                 ,
                    t2.client_secret             ,
                    t4.refresh_token             " ;
        $_sql .= " FROM {$_databases}.{$_tables} as t1 " ;
        $_sql .= " LEFT JOIN OAUTH.oauth_clients t2 ON (t1.{$_field} = t2.user_id) " ;
//        $_sql .= " LEFT JOIN OAUTH.oauth_public_keys t3 ON (t2.client_id = t3.client_id) " ;
        $_sql .= " LEFT JOIN (
                    SELECT
                        *
                    FROM OAUTH.oauth_refresh_tokens
                    WHERE user_id = '".$id."'
                    AND scope = '".$type."'
                    ORDER BY expires DESC LIMIT 0,1
        ) as t4 ON (t2.client_id = t4.client_id AND t2.user_id = t4.user_id) " ;
        $_sql .= " WHERE t1.{$_field} = '".$id."' " ;
        $_sql .= " AND t2.scope = '".$type."'     " ;

        $_res = $this->db->query($_sql) ;
        return $_res->row_array() ;
    }


    public function getAccessToken($type){
        $_client       = $this->getClient($this->infoObj->user_id,$type) ;
        $url           = TOKEN_URI                               ;
        $client_id     = $_client['client_id']                   ;
        $client_secret = $_client['client_secret']               ;
        $_param = array(
            "grant_type"    => "password"                ,
            "username"      => $this->infoObj->user_id   ,
            "password"      => $this->infoObj->user_pass ,
            "client_id"     => $client_id                ,
            "client_secret" => $client_secret            ,
            "scope"         => $type
        ) ;
        $tokenContent   = http_build_query($_param)              ;
//        $authorization  = $client_id.":".$client_secret          ;
        $tokenHeaders   = array(
            "Content-Type: application/x-www-form-urlencoded")   ;
        $token = curl_init()                                            ;
        curl_setopt($token, CURLOPT_URL, $url)                   ;
        curl_setopt($token, CURLOPT_HTTPHEADER, $tokenHeaders)   ;
        curl_setopt($token, CURLOPT_SSL_VERIFYPEER, false) ;
        curl_setopt($token, CURLOPT_RETURNTRANSFER, true)  ;
        curl_setopt($token, CURLOPT_POST, true)            ;
        curl_setopt($token, CURLOPT_POSTFIELDS, $tokenContent)   ;
        $response = curl_exec($token)                                   ;
        curl_close ($token) ;

        echo $url.chr(10);
        print_r($tokenHeaders);
        echo $tokenContent.chr(10);

        return $response    ;
    }

    // 리프레쉬토큰을 통한 신규 access token 발급
    public function sendAccessToken($token){
        $_client       = $this->getClient($token['user_id'],$token['scope']) ;
        $url           = TOKEN_URI                               ;
        $client_id     = $_client['client_id']                   ;
        $client_secret = $_client['client_secret']               ;
        $_param = array(
            "grant_type"    => "refresh_token"                   ,
            "refresh_token" => $_client['refresh_token']         ,
            "client_id"     => $client_id                        ,
            "client_secret" => $client_secret
        ) ;
        $tokenContent   = http_build_query($_param)              ;
        $tokenHeaders   = array(
            "Content-Type: application/x-www-form-urlencoded")   ;
        $token = curl_init()                                            ;
        curl_setopt($token, CURLOPT_URL, $url)                   ;
        curl_setopt($token, CURLOPT_HTTPHEADER, $tokenHeaders)   ;
        curl_setopt($token, CURLOPT_SSL_VERIFYPEER, false) ;
        curl_setopt($token, CURLOPT_RETURNTRANSFER, true)  ;
        curl_setopt($token, CURLOPT_POST, true)            ;
        curl_setopt($token, CURLOPT_POSTFIELDS, $tokenContent)   ;
        $response = curl_exec($token)                                   ;
        curl_close ($token) ;
        return $response    ;
    }


    // 검증 및 api서버 통신
    public function sendAuth($_param,$type){
        $url            = TOKEN_AUTH                              ;
        $tokenContent   = http_build_query($_param)               ;
        $tokenHeaders   = array(
            "Content-Type: application/x-www-form-urlencoded")   ;
        $token = curl_init()       ;
        curl_setopt($token, CURLOPT_URL, $url)                   ;
        curl_setopt($token, CURLOPT_HTTPHEADER, $tokenHeaders)   ;
        curl_setopt($token, CURLOPT_SSL_VERIFYPEER, false) ;
        curl_setopt($token, CURLOPT_RETURNTRANSFER, true)  ;
        curl_setopt($token, CURLOPT_POST, true)            ;
        curl_setopt($token, CURLOPT_POSTFIELDS, $tokenContent)   ;
        $response = curl_exec($token)                                   ;
        curl_close ($token) ;

        try{
            // 토큰 갱신
            $_res = json_decode($response,true) ;
            if($_res['code'] == 200){
                $this->session->userdata[$type]['token'] = $_res['data']['oauth'] ;
            }

            // 넘어온 토큰값이 없다면 기간 만료
            if(!($_res['session'])){
                throw new Exception("세션기간 만료",403) ;
            }
        }catch (Exception $e){
            unset($_SESSION[$type])      ;
            exit ;
        }
        return $response    ;
    }

    public function callAPI($info,$token){
        $method = $info['method']                                  ;
        $url    = $info['redirect_uri']                            ;
        $data   = json_decode(base64_decode($info['param']),true)  ;

        $curl = curl_init();
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, true);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;

            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;

            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'API_KEY:'.$token['data']                 ,
            'Content-Type: application/x-www-form-urlencoded' ,
            'Accept:*/*'
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }


    public function addSession($type,$token){

        try{
            $_client = $this->getClient($this->infoObj->user_id,$type) ;
            $USER_DATA = array(
                $type       => array(
                    "user"  => array(
                        "code" => $_client['user_code']  ,
                        "id"   => $_client['user_id']    ,
                        "name" => $_client['user_name']
                    ) ,
                    "token" => $token['access_token']
                )
            ) ;
            $this->session->set_userdata($USER_DATA) ;

            $_code = 200               ;
            $_msg  = "세션 데이터 생성 성공" ;
        }catch (Exception $e){
            $_code = $e->getCode()    ;
            $_msg  = $e->getMessage() ;
        }

        $_res = array(
            "code" => $_code ,
            "msg"  => $_msg
        ) ;

        return $_res ;
    }




    // RSA 개인키, 공개키 조합을 생성한다.
    // 키 생성에는 시간이 많이 걸리므로, 한 번만 생성하여 저장해 두고 사용하면 된다.
    // 단, 비밀번호는 개인키를 사용할 때마다 필요하다.
    public function rsa_generate_keys($password=NULL, $bits = 2048, $digest_algorithm = 'sha256'){
        $key = array(
            'digest_alg' => $digest_algorithm,
            'private_key_bits' => $bits,
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
        ) ;
        $res = openssl_pkey_new($key) ;
        openssl_pkey_export($res, $private_key, $password);
        $public_key = openssl_pkey_get_details($res);
        $public_key = $public_key['key'];
        return array(
            'private_key' => $private_key,
            'public_key' => $public_key,
        ) ;
    }


    // RSA 공개키를 사용하여 문자열을 암호화한다.
    // 암호화할 때는 비밀번호가 필요하지 않다.
    // 오류가 발생할 경우 false를 반환한다.
    public function rsa_encrypt($plaintext, $public_key){
        // 용량 절감과 보안 향상을 위해 평문을 압축한다.

        $plaintext = gzcompress($plaintext);

        // 공개키를 사용하여 암호화한다.

        $pubkey_decoded = @openssl_pkey_get_public($public_key);
        if ($pubkey_decoded === false) return false;

        $ciphertext = false;
        $status = @openssl_public_encrypt($plaintext, $ciphertext, $pubkey_decoded);
        if (!$status || $ciphertext === false) return false;

        // 암호문을 base64로 인코딩하여 반환한다.

        return base64_encode($ciphertext);
    }

    // RSA 개인키를 사용하여 문자열을 복호화한다.
    // 복호화할 때는 비밀번호가 필요하다.
    // 오류가 발생할 경우 false를 반환한다.
    public function rsa_decrypt($ciphertext, $private_key, $password=NULL){
        // 암호문을 base64로 디코딩한다.

        $ciphertext = @base64_decode($ciphertext, true);
        if ($ciphertext === false) return false;

        // 개인키를 사용하여 복호화한다.

        $privkey_decoded = @openssl_pkey_get_private($private_key, $password);
        if ($privkey_decoded === false) return false;

        $plaintext = false;
        $status = @openssl_private_decrypt($ciphertext, $plaintext, $privkey_decoded);
        @openssl_pkey_free($privkey_decoded);
        if (!$status || $plaintext === false) return false;

        // 압축을 해제하여 평문을 얻는다.

        $plaintext = @gzuncompress($plaintext);
        if ($plaintext === false) return false;

        // 이상이 없는 경우 평문을 반환한다.

        return $plaintext;
    }

    public function getUser($id,$type){

        switch ($type){

            case "_ADMIN" :

                $_sql = " SELECT
                    t1.adm_id as user_id         ,
                    t1.adm_name  as user_name    ,
                    t1.adm_code as user_code     " ;
                $_sql .= " FROM IEQMS_ADM.TBL_ADM as t1 " ;
                $_sql .= " WHERE t1.adm_id = '".$id."' " ;
                $_res = $this->db->query($_sql) ;
                $_row = $_res->row_array() ;
                break ;

            case "_USER" :
                break ;

        }
        return $_row ;
    }

    // 전태룡 메뉴구조
    public function getMenu(){

        $menu[0]["name"]="컨텐츠관리";
        $menu[0]["id"]="content";
        $menu[0]["url"]="";
        $menu[0]["menu"][0]["name"]="문제등록 및 관리";
        $menu[0]["menu"][0]["id"]="question";
        $menu[0]["menu"][0]["url"]="";
        $menu[0]["menu"][1]["name"]="내신기출, 모의수능사관기출및 PSET 시험지";
        $menu[0]["menu"][1]["id"]="pape";
        $menu[0]["menu"][1]["url"]="";
        $menu[0]["menu"][2]["name"]="개념관리";
        $menu[0]["menu"][2]["id"]="concept";
        $menu[0]["menu"][2]["url"]="";
        $menu[0]["menu"][3]["name"]="단원유형";
        $menu[0]["menu"][3]["id"]="unit";
        $menu[0]["menu"][3]["url"]="";
        $menu[0]["menu"][4]["name"]="분류관리";
        $menu[0]["menu"][4]["id"]="type";
        $menu[0]["menu"][4]["url"]="";
        $menu[0]["menu"][5]["name"]="문제등록 및 관리";
        $menu[0]["menu"][5]["id"]="qr";
        $menu[0]["menu"][5]["url"]="QR코드";


        $menu[1]["name"]="통합관리";
        $menu[1]["id"]="integrated";
        $menu[1]["url"]="";
        $menu[1]["menu"][0]["name"]="학교관리";
        $menu[1]["menu"][0]["id"]="school";
        $menu[1]["menu"][0]["url"]="";
        $menu[1]["menu"][1]["name"]="회원관리";
        $menu[1]["menu"][1]["id"]="academy";
        $menu[1]["menu"][1]["url"]="";
        $menu[1]["menu"][2]["name"]="권한설정";
        $menu[1]["menu"][2]["id"]="auth";
        $menu[1]["menu"][2]["url"]="";
        $menu[1]["menu"][3]["name"]="운영자관리";
        $menu[1]["menu"][3]["id"]="admin";
        $menu[1]["menu"][3]["url"]="";


        $menu[2]["name"]="시험관리";
        $menu[2]["id"]="exam";
        $menu[2]["url"]="";
        $menu[2]["menu"][0]["name"]="시험지생성";
        $menu[2]["menu"][0]["id"]="create";
        $menu[2]["menu"][0]["url"]="";
        $menu[2]["menu"][1]["name"]="성적처리 및 클리닉";
        $menu[2]["menu"][1]["id"]="scoreclinic";
        $menu[2]["menu"][1]["url"]="";


        $menu[3]["name"]="고객지원관리";
        $menu[3]["id"]="customer";
        $menu[3]["url"]="";
        $menu[3]["menu"][0]["name"]="문제오류접수";
        $menu[3]["menu"][0]["id"]="error";
        $menu[3]["menu"][0]["url"]="";
        $menu[3]["menu"][1]["name"]="컨텐츠 등록내역";
        $menu[3]["menu"][1]["id"]="contentlist";
        $menu[3]["menu"][1]["url"]="";
        $menu[3]["menu"][2]["name"]="1:1상담내역";
        $menu[3]["menu"][2]["id"]="onenone";
        $menu[3]["menu"][2]["url"]="";


        $menu[4]["name"]="D-test";
        $menu[4]["id"]="dtest";
        $menu[4]["url"]="";
        $menu[4]["menu"][0]["name"]="MRI";
        $menu[4]["menu"][0]["id"]="mri";
        $menu[4]["menu"][0]["url"]="";
        $menu[4]["menu"][1]["name"]="MCT";
        $menu[4]["menu"][1]["id"]="mct";
        $menu[4]["menu"][1]["url"]="";
        $menu[4]["menu"][2]["name"]="PSET";
        $menu[4]["menu"][2]["id"]="pset";
        $menu[4]["menu"][2]["url"]="";

        return $menu ;
    }
}
