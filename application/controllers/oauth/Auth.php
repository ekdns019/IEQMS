<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    // 접속 허용 ip
    protected $allowIp = array(
        "220.85.81.174"   ,
        "211.117.60.23"   ,
        "211.117.60.108"  ,
        "112.167.100.175" ,
        "125.179.235.241" , 
        "127.0.0.1"
    ) ;

    // 리디렉션 허용 method
    protected $method = array(
        "GET"    ,
        "POST"   ,
        "PUT"    ,
        "DELETE" ,
        "PATCH"
    ) ;

    protected $oauth ;

    public function __construct(){
        parent::__construct() ;
        $this->load->library("server")               ;
        $this->infoObj  = new stdClass()                   ;
        $this->oauth    = $this->server->_server()         ;
        $this->serverIp = $_SERVER['REMOTE_ADDR']          ;
        try{
            $this->server->_delete() ; // 유효기간 지난 데이터 삭제

            if(empty($this->serverIp)){
                throw new Exception("올바르지 않은 접근입니다.",403) ;
            }
            if(!(in_array($this->serverIp,$this->allowIp))){
                throw new Exception("접근권한이 없는 아이피입니다.",403) ;

            }
            $this->code = 200              ;
            $this->msg  = "기본 유효성검사 통과" ;
        }catch (Exception $e){
            $this->code = $e->getCode()    ;
            $this->msg  = $e->getMessage() ;
        }

        // 유효성검사 미 통과시 요청종료
        if($this->code != 200){
            $_res = array(
                "code" => $this->code ,
                "error"  => $this->msg ,
                "error_description" => $this->msg
            ) ;
            echo json_encode($_res,JSON_UNESCAPED_UNICODE) ;
            exit ;
        }

    }

    // 엑세스토큰 가져오기
    public function accessToken(){
        $this->oauth->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
    }


    // 중계
    public function response(){

        // 넘어온 변수값 출력
        $_request = OAuth2\Request::createFromGlobals() ;

        try{
            // 넘어온 엑세스 토큰값이 없을때
            if(empty($_request->request['access_token'])){
                $_error = array(
                    "error"             => "invalid_token" ,
                    "error_description" => "Not found access_token"
                ) ;
                throw new Exception(json_encode($_error),403) ;
            }

            // 넘어온 엑세스 토큰값이 정상적이지 않을때
            if (!$this->oauth->verifyResourceRequest($_request)) {
                $_error = $this->oauth->getResponse() ;
                throw new Exception(json_encode($_error->parameters),400) ;
            }

            // redirect uri가 없을때
            if(empty($_request->request['redirect_uri'])){
                $_error = array(
                    "error"             => "invalid_request" ,
                    "error_description" => "Not found redirect_uri"
                ) ;
                throw new Exception(json_encode($_error),403) ;
            }

            // redirect uri가 정상적이지 않을때
            if(!(strpos($_request->request['redirect_uri'], "http://bank.ps-e.co.kr/api") !== false)) {
                $_error = array(
                    "error"             => "invalid_request" ,
                    "error_description" => "Invalid redirect_uri"
                ) ;
                throw new Exception(json_encode($_error),403) ;
            }

            // 재호출할 URI METHOD가 없을때
            if(empty($_request->request['method'])){
                $_error = array(
                    "error"             => "invalid_request" ,
                    "error_description" => "Not found method"
                ) ;
                throw new Exception(json_encode($_error),403) ;
            }

            // 허용되지 않은 METHOD 선언시
            if(!(in_array($_request->request['method'], $this->method))) {
                $_error = array(
                    "error"             => "invalid_request" ,
                    "error_description" => "Invalid method"
                ) ;
                throw new Exception(json_encode($_error),403) ;
            }

            // 호출 method가 GET이 아니고, param이 비어있을때
            if($_request->request['method'] != "GET"){
                if(empty($_request->request['param'])){
                    $_error = array(
                        "error"             => "invalid_request" ,
                        "error_description" => "Not found Param"
                    ) ;
                    throw new Exception(json_encode($_error),403) ;
                }
            }


            // 토큰 정보
            $this->infoObj->token['old'] = $this->oauth->getAccessTokenData(OAuth2\Request::createFromGlobals())                                            ;
            $this->infoObj->time['reg']  = $this->infoObj->token['old']['iat']                                                                              ;
            $this->infoObj->time['exp']  = $this->infoObj->token['old']['expires']                                                                          ;
            $this->infoObj->time['now']  = time()                                                                                                           ;
            $this->infoObj->time['ref']  = ceil(($this->infoObj->time['exp'] - $this->infoObj->time['reg']) / 2)                                      ;
            $this->infoObj->token['new'] = $this->refreshAccessToken($this->infoObj->time,$this->infoObj->token['old'],$_request->request['access_token'])  ;




            // 리다이렉션 uri 호출
            $_api = json_decode($this->callAPI($_request->request,$this->infoObj->token['new']),true) ;

            $_code  = 200         ;
            $_error = array()    ;
            $_msg   = "요청결과 반환" ;
            $_data  = array(
                "oauth" => $this->infoObj->token['new']['data'] ,
                "api"   => $_api['data']
            ) ;
            $_desc = array(
                "oauth" => array(
                    "code" => $this->infoObj->token['new']['code'] ,
                    "msg"  => $this->infoObj->token['new']['msg']  ,
                ) ,
                "api"   => array(
                    "code" => $_api['code'] ,
                    "msg"  => $_api['msg']  ,
                )
            ) ;
            $_session = true ;

        }catch (Exception $e){
            $_code    = $e->getCode()                            ;
            $_error   = json_decode($e->getMessage(),true) ;
            $_msg     = $_error['error']                         ;
            $_data    = array()                                  ;
            $_desc    = array()                                  ;

            // 토큰정보 확인
            if($_code == "400"){
                $_session = false ;
            }else{
                $__token = $this->oauth->getAccessTokenData(OAuth2\Request::createFromGlobals()) ;
                $_session = $__token['expires'] > time() ? true : false ;
            }

        }finally{
            $_res = array(
                "code"    => $_code   ,
                "msg"     => $_msg    ,
                "data"    => $_data   ,
                "desc"    => $_desc   ,
                "session" => $_session
            ) ;
            echo  json_encode($_res,JSON_UNESCAPED_UNICODE) ;
        }
    }



    // 특정 조건이 만족되었을때 신규 accessToken 발급
    private function refreshAccessToken($time,$token,$old){

        $_now = $time['now'] ; // 현재시간
        $_reg = $time['reg'] ; // 등록시간
        $_exp = $time['exp'] ; // 만료시간
        $_ref = $time['ref'] ; // 생성주기

        try{

            // 현재시간이 만료시간보다 크다면
            if($_now > $_exp){
                throw new Exception("만료시간 경과 토큰",500) ;
            }

            // 현재시간 - 생성시간이 생성주기보다 작을때
            if($_now - $_reg < $_ref){
                throw new Exception("토큰 생성주기 미달",500) ;
            }


            // 신규 토큰 생성 ( 생성주기가 경과했고, 만료되지는 않았다. )
            $_new = json_decode($this->sendAccessToken($token),true) ;

            if(empty($_new['access_token'])){
                throw new Exception($_new['error_description'],500) ;
            }

            $_code = 200                   ;
            $_msg  = "신규 토큰발급 성공"       ;
            $_data = $_new['access_token'] ;

        }catch (Exception $e){
            $_code = $e->getCode()    ;
            $_msg  = $e->getMessage() ;
            $_data = $old             ;
        }

        $_res = array(
            "code" => $_code ,
            "msg"  => $_msg  ,
            "data" => $_data
        ) ;
        return $_res ;
    }
}
