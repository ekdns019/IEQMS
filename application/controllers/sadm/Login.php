<?php
require APPPATH."/libraries/SADM_Controller.php" ;
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends SADM_Controller {
    public function __construct(){
        parent::__construct() ;
    }

    public function index(){
        $this->setDefault(false) ;
        $this->load->view("/".$this->__SEG[1]."/login") ;

    }
    public function login(){

        $this->setDefault(false) ;

        $this->infoObj->user_id   = $this->input->post('userid',true)  ; // 사용자 id
        $this->infoObj->user_pass = $this->input->post('userpwd',true) ; // 사용자 pwd

        try{
            // 먼저 access token 발급
            $curlResponse = $this->getAccessToken($this->_sessionNm) ;
            // 토큰정보
            $token = json_decode($curlResponse,true) ;

            // 만약 토큰 정보에 access token이 없다면
            if(empty($token['access_token'])){
                exit;
                throw new Exception($token['error_description'],500) ;
            }

            // 세션을 등록
            $_session = $this->addSession($this->_sessionNm,$token) ;

            // 세션등록에 실패한 경우
            if($_session != 200){
                throw new Exception($_session['msg'],$_session['code']) ;
            }

            $_code = $_session['code'] ;
            $_msg  = $_session['msg']  ;
        }catch (Exception $e){
            $_code = $e->getCode()    ;
            $_msg  = $e->getMessage() ;
        }
        if($_code != 200){
            alert_js("alert_parent_reload",$_msg,"") ;
        }else{
            alert_js("parent_move","",base_url().$this->firstPage) ;
        }
    }


    public function logout(){
        $this->setDefault(is_bool(true)) ;
        unset($_SESSION[$this->_sessionNm])    ;
        $this->ducommon->alert_js("parent_move","",base_url().$this->firstPage) ;
        exit ;
    }
}
