<?php
require APPPATH."/libraries/SADM_Controller.php" ;
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends SADM_Controller {
    public function __construct(){
        parent::__construct()                ;
        $this->setDefault(true) ;
    }

    public function index(){
        redirect(base_url()."sadm/main/main") ;
    }

    public function main(){
        $this->loadPage("/sadm/main/main", "", array("top", "header", "footer", "bottom"), "Y,Y,N,Y") ;
    }

}
