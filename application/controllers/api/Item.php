<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Item extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->library("server")               ;
        $this->oauth    = $this->server->_server()         ;
        $this->serverIp = $_SERVER['REMOTE_ADDR']          ;

    }
    public function index_get(){





//        echo print_r($this->server);
        $users = array(
            array(
                'id' => 0, 'name' => 'John', 'email' => 'john@example.com'
            ) ,
            array(
                'id' => 1, 'name' => 'Jim', 'email' => 'jim@example.com'
            )
        ) ;

        $this->searchObj->search = $this->uri->uri_to_assoc(3) ;
        $this->searchObj->search['id'] = empty($this->searchObj->search['id']) || $this->searchObj->search['id'] === null ? null : $this->searchObj->search['id'] ;
        $id = $this->searchObj->search['id'] ;

        // GET param
        if ( $id === null ){

            if ( $users ) {
                $_res = array(
                    "code" => 200            ,
                    "msg"  => "데이터 호출 성공" ,
                    "type" => "success"      ,
//                    "data" => $users
                    "data" => $this->user
                ) ;
                $this->response( $_res, 200 );


            } else {
                // Set the response and exit
                $_res = array(
                    "code" => 404            ,
                    "msg"  => "데이터 없음"     ,
                    "type" => "danger"       ,
                    "data" => array()
                ) ;
                $this->response($_res,404 );
            }
        } else {
            if ( array_key_exists( $id, $users ) ) {
                $_res = array(
                    "code" => 200            ,
                    "msg"  => "데이터 호출 성공" ,
                    "type" => "success"      ,
                    "data" => $users[$id]
                ) ;

                $this->response($_res, 200 );
            } else {
                // Set the response and exit
                $_res = array(
                    "code" => 404            ,
                    "msg"  => "데이터 없음"     ,
                    "type" => "danger"       ,
                    "data" => array()
                ) ;
                $this->response($_res,404 );
            }
        }
    }








    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_post(){
        $input = $this->input->post();
        $_res = array(
            "code" => 200 ,
            "msg" => "데이터 등록 성공 (POST값만 반환)" ,
            "type" => "success" ,
            "data" => $input
        ) ;

        $this->response($_res, 200);
    }



    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_put(){

        $input = $this->put() ;
        $users = array(
            array(
                'id' => 0, 'name' => 'John', 'email' => 'john@example.com'
            ) ,
            array(
                'id' => 1, 'name' => 'Jim', 'email' => 'jim@example.com'
            )
        ) ;

        $this->searchObj->search = $this->uri->uri_to_assoc(3) ;
        $this->searchObj->search['id'] = empty($this->searchObj->search['id']) || $this->searchObj->search['id'] === null ? null : $this->searchObj->search['id'] ;
        $id = $this->searchObj->search['id'] ;

//        echo $id ;

        $users[$input['id']] = array(
            "id"   => $input['id']    ,
            'name' => $input['name']  ,
            'email' => $input['email']
        ) ;

        $_res = array(
            "code" => 200            ,
            "msg"  => "데이터 수정 성공" ,
            "type" => "success"      ,
            "data" => $users[$input['id']]
        ) ;
        $this->response($_res, 200);
    }


    public function index_delete(){
        $users = array(
            array(
                'id' => 0, 'name' => 'John', 'email' => 'john@example.com'
            ) ,
            array(
                'id' => 1, 'name' => 'Jim', 'email' => 'jim@example.com'
            )
        ) ;

        $this->searchObj->search = $this->uri->uri_to_assoc(3) ;
        $this->searchObj->search['id'] = empty($this->searchObj->search['id']) || $this->searchObj->search['id'] === null ? null : $this->searchObj->search['id'] ;
        $id = $this->searchObj->search['id'] ;

        unset($users[$id]) ;


        $_res = array(
            "code" => 200            ,
            "msg"  => "데이터 삭제 성공" ,
//            "msg"  => $this->token ,
            "type" => "success"      ,
            "data" => $users
        ) ;
        $this->response($_res, 200);
    }



}
