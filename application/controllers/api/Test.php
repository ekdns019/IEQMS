<?php
error_reporting(E_ALL); // Display errors in output
ini_set('display_errors', 1);


defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Test extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
        //$this->load->model("common/Common_model","common_model")      ; // 공통모델

        $this->load->model('common/Createkey_model','createkey_model') ; // 모델 호출
        $this->load->model('api/Adm_model','adm_model') ;                // 모델 호출
        $this->load->model('api/Auth_model','auth_model') ;                // 모델 호출

    }

    public function index_get(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "admin";
        $auth["_auth_name"][2] = "r";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        if(isset($this->uri->segments[3])){
            $gbn = "view";
            $data["adm_code"] = $this->uri->segments[3];
        }else{
            $gbn="list";
        }

        $data["search"]      = $this->input->get('search',true)         ; // 검색사용 on
        $data["seartext"]    = $this->input->get('seartext',true)         ; // 검색어
        $data["gp"]          = $this->input->get('gp',true)             ; // 페이징
        $data["ps"]          = $this->input->get('ps',true)             ; // 페이징
        $data["ord"]         = $this->input->get('ord',true)            ; // 정렬
        $data["ord_type"]    = $this->input->get('ord_type',true)      ; // 정렬차순

        $data["page_state"]    = "on"      ; //페이징 처리 (on 사용)

        if($data["page_state"] == "on") {
            if($data["gp"]==""){ $data["gp"]=1; }
            if($data["ps"]==""){ $data["ps"]=10; }
        }

        if($gbn=="list"){
            $data['list'] = json_decode($this->adm_model->get_list_adm($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "운영자_리스트"     ,
                "type" => "success"             ,
                "data" => array("list" => $data['list'])
            ) ;
            $this->response($_res,200 );

        }elseif($gbn=="view"){
            $data['view'] = json_decode($this->adm_model->get_view_adm($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "운영자_상세보기"     ,
                "type" => "success"             ,
                "data" => array("view" => $data['view'])
            ) ;
            $this->response($_res,200 );
        }

    }


    public function index_post(){

        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "admin";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["adm_id"]       = $this->input->get('adm_id',true)         ; // *아이디
        $data["adm_pwd"]      = $this->input->get('adm_pwd',true)         ; // *비번
        $data["adm_name"]     = $this->input->get('adm_name',true)        ; // *이름
        $data["adm_email"]    = $this->input->get('adm_email',true)       ; // 이메일
        $data["adm_phone"]    = $this->input->get('adm_phone',true)       ; // 연락처
        $data["ma_code"]      = $this->input->get('ma_code',true)         ; // *권한코드
        $data["p_code"]       = $this->input->get('p_code',true)          ; // *직급코드


        if(trim($data["adm_id"])=="" || trim($data["adm_pwd"])=="" || trim($data["adm_name"])=="" || trim($data["ma_code"])=="" || trim($data["p_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->adm_model->post_adm($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_put(){

        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "admin";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["adm_code"]       = $this->input->get('adm_code',true)         ; // *키값
        $data["adm_pwd_change"]      = $this->input->get('adm_pwd_change',true)         ; // 변경비번
        $data["adm_name"]     = $this->input->get('adm_name',true)        ; // *이름
        $data["adm_email"]    = $this->input->get('adm_email',true)       ; // 이메일
        $data["adm_phone"]    = $this->input->get('adm_phone',true)       ; // 연락처
        $data["ma_code"]      = $this->input->get('ma_code',true)         ; // *권한코드
        $data["p_code"]       = $this->input->get('p_code',true)          ; // *직급코드


        if(trim($data["adm_code"])=="" || trim($data["adm_pwd_change"])=="" || trim($data["adm_name"])=="" || trim($data["ma_code"])=="" || trim($data["p_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->adm_model->put_adm($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_delete(){

        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "admin";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["adm_code"]        = $this->input->get('adm_code',true)         ; // 키값

        if(trim($data["adm_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->adm_model->delete_adm($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


}
