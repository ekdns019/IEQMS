<?php
error_reporting(E_ALL); // Display errors in output
ini_set('display_errors', 1);


defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Codeagency extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
        //$this->load->model("common/Common_model","common_model")      ; // 공통모델

        $this->load->model('common/Createkey_model','createkey_model') ; // 모델 호출
        $this->load->model('api/Codemanage_model','codemanage_model') ; // 모델 호출

    }

    public function index_get(){

        if(isset($this->uri->segments[3])){
            $gbn = "view";
            $data["p_code"] = $this->uri->segments[3];
        }else{
            $gbn="list";
            $data["p_code"] = "";
        }


        $data["search"]      = $this->input->get('search',true)         ; // 검색어
        $data["gp"]          = $this->input->get('gp',true)             ; // 페이징
        $data["ps"]          = $this->input->get('ps',true)             ; // 페이징
        $data["ord"]         = $this->input->get('ord',true)            ; // 정렬
        $data["ord_type"]    = $this->input->get('ord_type ',true)      ; // 정렬차순

        if($gbn=="list"){

            $data['list'] = json_decode($this->codemanage_model->get_list_position($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "코드_기관_리스트"     ,
                "type" => "success"             ,
                "data" => array("list" => $data['list'])
            ) ;
            $this->response($_res,200 );

        }elseif($gbn=="view"){
            $data['view'] = json_decode($this->codemanage_model->get_view_position($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "코드_기관_상세보기"     ,
                "type" => "success"             ,
                "data" => array("view" => $data['view'])
            ) ;
            $this->response($_res,200 );

        }

    }


    public function index_post(){
        $data["p_name"]      = $this->input->post('p_name',true)         ; // 직급명
        $data["p_state"]      = $this->input->post('p_state',true)         ; // 사용여부
        if($data["p_state"]==""){ $data["p_state"]="Y"; }

        if(trim($data["p_name"])=="" || trim($data["p_state"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->post_position($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_put(){
        $data["p_code"]        = $this->input->put('p_code',true)         ; // 직급코드
        $data["p_name"]        = $this->input->put('p_name',true)         ; // 직급명
        $data["p_state"]      = $this->input->put('p_state',true)       ; // 사용여부
        if($data["p_state"]==""){ $data["p_state"]="Y"; }

        if(trim($data["p_code"])=="" || trim($data["p_name"])=="" || trim($data["p_state"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->put_position($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


    public function index_delete(){
        $data["p_code"]        = $this->input->delete('p_code',true)         ; // 직급명

        if(trim($data["p_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->delete_position($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


}
