<?php
error_reporting(E_ALL); // Display errors in output
ini_set('display_errors', 1);


defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Codedepartment extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
        //$this->load->model("common/Common_model","common_model")      ; // 공통모델

        $this->load->model('common/Createkey_model','createkey_model') ; // 모델 호출
        $this->load->model('api/Codemanage_model','codemanage_model') ; // 모델 호출
        $this->load->model('api/Auth_model','auth_model') ; // 모델 호출
    }

    public function index_get(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "content";
        $auth["_auth_name"][1] = "type";
        $auth["_auth_name"][2] = "r";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        if(isset($this->uri->segments[3])){
            $gbn = "view";
            $data["cd_code"] = $this->uri->segments[3];
        }else{
            $gbn="list";
            $data["cd_code"] = "";
        }


        $data["search"]      = $this->get('search',true)         ; // 검색어
        $data["gp"]          = $this->get('gp',true)             ; // 페이징
        $data["ps"]          = $this->get('ps',true)             ; // 페이징
        $data["ord"]         = $this->get('ord',true)            ; // 정렬
        $data["ord_type"]    = $this->get('ord_type ',true)      ; // 정렬차순

        if($gbn=="list"){

            $data['list'] = json_decode($this->codemanage_model->get_list_codedepartment($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "코드_계열_리스트"     ,
                "type" => "success"             ,
                "data" => array("list" => $data['list'])
            ) ;
            $this->response($_res,200 );

        }elseif($gbn=="view"){
            $data['view'] = json_decode($this->codemanage_model->get_view_codedepartment($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "코드_계열_상세보기"     ,
                "type" => "success"             ,
                "data" => array("view" => $data['view'])
            ) ;
            $this->response($_res,200 );

        }

    }


    public function index_post(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "content";
        $auth["_auth_name"][1] = "type";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["cd_subject"]      = $this->post('cd_subject',true)         ; // 계열명

        if(trim($data["cd_subject"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->post_codedepartment($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_put(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "content";
        $auth["_auth_name"][1] = "type";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        if(isset($this->uri->segments[3])){            // 계열코드
            $data["cd_code"] = $this->uri->segments[3];
        }else{
            $data["cd_code"] = "";
        }

        $data["cd_subject"]      = $this->put('cd_subject',true)       ; // 계열명

        if(trim($data["cd_code"])=="" || trim($data["cd_subject"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->put_codedepartment($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


    public function index_delete(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "content";
        $auth["_auth_name"][1] = "type";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["cd_code"]        = $this->delete('cd_code',true)         ; // 계열키값

        if(trim($data["cd_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->delete_codedepartment($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


}
