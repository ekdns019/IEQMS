<?php
error_reporting(E_ALL); // Display errors in output
ini_set('display_errors', 1);


defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Menu extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
        //$this->load->model("common/Common_model","common_model")      ; // 공통모델

        $this->load->model('common/Createkey_model','createkey_model') ; // 모델 호출
        $this->load->model('api/Menu_model','menu_model') ;                // 모델 호출
        $this->load->model('api/Auth_model','auth_model') ; // 모델 호출
    }

    public function index_get(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "auth";
        $auth["_auth_name"][2] = "r";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        if(isset($this->uri->segments[3])){
            $gbn = "view";
            $data["ma_code"] = $this->uri->segments[3];
        }else{
            $gbn="list";
        }

        $data["search"]      = $this->input->get('search',true)         ; // 검색사용 on
        $data["seartext"]    = $this->input->get('seartext',true)         ; // 검색어
        $data["gp"]          = $this->input->get('gp',true)             ; // 페이징
        $data["ps"]          = $this->input->get('ps',true)             ; // 페이징
        $data["ord"]         = $this->input->get('ord',true)            ; // 정렬
        $data["ord_type"]    = $this->input->get('ord_type',true)      ; // 정렬차순

        $data["page_state"]    = ""      ; //페이징 처리 (on 사용)

        if($data["page_state"] == "on") {
            if($data["gp"]==""){ $data["gp"]=1; }
            if($data["ps"]==""){ $data["ps"]=10; }
        }

        if($gbn=="list"){
            $data['list'] = json_decode($this->menu_model->get_list_menuauth($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "권한_목록"     ,
                "type" => "success"             ,
                "data" => array("list" => $data['list'])
            ) ;
            $this->response($_res,200 );

        }elseif($gbn=="view"){
            $data['view'] = json_decode($this->menu_model->get_view_menuauth($data),true)                 ;

            $_res = array(
                "code" => 200                   ,
                "msg"  => "권한_상세보기"     ,
                "type" => "success"             ,
                "data" => array("view" => $data['view'])
            ) ;
            $this->response($_res,200 );
        }

    }


    public function index_post(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "auth";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["ma_groupname"]       = $this->input->post('ma_groupname',true)         ; //메뉴그룹명

        if(trim($data["ma_groupname"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->menu_model->post_menuauth($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_put(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "auth";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["ma_code"]       = $this->input->put('ma_code',true)         ; // *키값
        $data["ma_groupname"]      = $this->input->put('ma_groupname',true)         ; // *메뉴그룹명
        $data["ma_admin_auth"]     = $this->input->put('ma_admin_auth',true)        ; // 관리자권한


        if(trim($data["adm_code"])=="" || trim($data["adm_pwd_change"])=="" || trim($data["adm_name"])=="" || trim($data["ma_code"])=="" || trim($data["p_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->menu_model->put_menuauth($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_delete(){
        //권한 초기화 시작
        $auth["_auth_name"][0] = "integrated";
        $auth["_auth_name"][1] = "auth";
        $auth["_auth_name"][2] = "w";

        $auth["user_code"] = $this->user["user_code"];
        $result = json_decode($this->auth_model->get_state_adm($auth),true)                 ;

        if($result["code"]<>200){
            $_res = array(
                "code" => 500                   ,
                "msg"  => $result["msg"]        ,
                "type" => $result["type"]       ,
                "data" => array()
            ) ;
            $this->response($_res,200 );
            exit;
        }
        //권한 초기화 종료

        $data["adm_code"]        = $this->input->delete('adm_code',true)         ; // 키값

        if(trim($data["adm_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->menu_model->delete_menuauth($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }

}
