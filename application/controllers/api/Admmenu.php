<?php
error_reporting(E_ALL); // Display errors in output
ini_set('display_errors', 1);


defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Admmenu extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
        //$this->load->model("common/Common_model","common_model")      ; // 공통모델

        $this->load->model('common/Createkey_model','createkey_model') ; // 모델 호출
        $this->load->model('api/Menu_model','menu_model') ;                // 모델 호출
        $this->load->model('api/Auth_model','auth_model') ; // 모델 호출
    }

    public function index_get(){

        if(isset($this->uri->segments[3])){
            $data["adm_code"] = $this->uri->segments[3];
        }else{
            $data["adm_code"] = "";
        }

        if(trim($data["adm_code"])==""){
            $_res = array(
                "code" => 550                     ,
                "msg"  => "필수 파라메터 오류"     ,
                "type" => "danger"                ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{

            $result = json_decode($this->auth_model->get_admmenu($data["adm_code"]),true)                 ;

            if($result["code"] <> 200){
                $_res = array(
                    "code" => $result["code"]        ,
                    "msg"  => $result["msg"]         ,
                    "type" => $result["type"]        ,
                    "data" => array()
                ) ;
                $this->response( $_res, 500 );

            }

            //$v 전체메뉴
            $v =  json_decode($this->getMenu(),true);

            //$d 내 메뉴와 권한
            $d = $result["data"];

            $main_num = 0;      //초기화
            //대메뉴
            for($i=0;$i<count($v);$i++){
                $sub_num = 0;       //초기화
                if(array_key_exists($v[$i]["id"],$d)){

                    $menu[$main_num]["name"] = $v[$i]["name"];
                    $menu[$main_num]["id"]=$v[$i]["id"];
                    $menu[$main_num]["url"]=$v[$i]["url"];

                    //중메뉴
                    for($j=0;$j<count($v[$i]["menu"]);$j++){
                        if(array_key_exists($v[$i]["menu"][$j]["id"],$d[$v[$i]["id"]])){

                            $menu[$main_num]["menu"][$sub_num]["name"]=$v[$i]["menu"][$j]["name"];
                            $menu[$main_num]["menu"][$sub_num]["id"]=$v[$i]["menu"][$j]["id"];
                            $menu[$main_num]["menu"][$sub_num]["url"]=$v[$i]["menu"][$j]["url"];
                            $menu[$main_num]["menu"][$sub_num]["access"]=$d[$v[$i]["id"]][$v[$i]["menu"][$j]["id"]];
                            $sub_num++;
                        }
                    }
                    $main_num++;
                }

            }

            $_res = array(
                "code" => 200                  ,
                "msg"  => "메뉴불러오기 성공"  ,
                "type" => "success"              ,
                "data" => array("menu"=> $menu)
            ) ;
            $this->response( $_res, 500 );


            $result = json_encdoe($menu);
            return $result;

        }


    }

}
