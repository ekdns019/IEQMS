<?php
error_reporting(E_ALL); // Display errors in output
ini_set('display_errors', 1);


defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Codemanage extends RestController {
    public function __construct(){
        parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
        //$this->load->model("common/Common_model","common_model")      ; // 공통모델

        $this->load->model('common/Createkey_model','createkey_model') ; // 모델 호출
        $this->load->model('api/Codemanage_model','codemanage_model') ; // 모델 호출

    }

    public function index_get(){

        if(isset($this->uri->segments[3])){
            $gbn = $this->uri->segments[3];
        }else{
            $gbn="";
        }

        $data["search"]      = $this->input->get('search',true)         ; // 검색어
        $data["gp"]          = $this->input->get('gp',true)             ; // 페이징
        $data["ps"]          = $this->input->get('ps',true)             ; // 페이징
        $data["ord"]         = $this->input->get('ord',true)            ; // 정렬
        $data["ord_type"]    = $this->input->get('ord_type ',true)      ; // 정렬차순

        if($gbn=="" || ($gbn<>"list" && $gbn<>"view")){
            //echo "ERR";
            $_res = array(
                "code" => 500           ,
                "msg"  => "구분자오류"   ,
                "type" => "false"       ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            if($gbn=="list"){

                $data['list'] = json_decode($this->codemanage_model->get_list_codeschool($data),true)                 ;

                $_res = array(
                    "code" => 200                   ,
                    "msg"  => "코드_학제_리스트"     ,
                    "type" => "success"             ,
                    "data" => array("list" => $data['list'])
                ) ;
                $this->response($_res,200 );

            }elseif($gbn=="view"){
                if(isset($this->uri->segments[4])){
                    $data["cs_code"] = $this->uri->segments[4];
                }else{
                    $data["cs_code"] = "";
                }

                if(trim($data["cs_code"])==""){
                    $_res = array(
                        "code" => 550              ,
                        "msg"  => "필수 파라메터 오류"   ,
                        "type" => "false"            ,
                        "data" => array()
                    ) ;
                    $this->response( $_res, 500 );

                }else{
                    $data['view'] = json_decode($this->codemanage_model->get_view_codeschool($data),true)                 ;

                    $_res = array(
                        "code" => 200                   ,
                        "msg"  => "코드_학제_상세보기"     ,
                        "type" => "success"             ,
                        "data" => array("view" => $data['view'])
                    ) ;
                    $this->response($_res,200 );
                }
            }
        }
    }


    public function index_post(){
        $data["cs_school"]      = $this->input->get('cs_school',true)         ; // 학제명

        if(trim($data["cs_school"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->post_codeschool($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }
            $this->response( $result, 200 );
        }
    }


    public function index_put(){
        $data["cs_code"]        = $this->input->get('cs_code',true)         ; // 학제명
        $data["cs_school"]      = $this->input->get('cs_school',true)       ; // 학제명

        if(trim($data["cs_code"])=="" || trim($data["cs_school"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->put_codeschool($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


    public function index_delete(){
        $data["cs_code"]        = $this->input->get('cs_code',true)         ; // 학제명

        if(trim($data["cs_code"])==""){
            $_res = array(
                "code" => 550                  ,
                "msg"  => "필수 파라메터 오류"  ,
                "type" => "false"              ,
                "data" => array()
            ) ;
            $this->response( $_res, 500 );

        }else{
            $result = json_decode($this->codemanage_model->delete_codeschool($data),true)                 ;

            if($result["code"]<>"200"){
                $result_code = 500;
            }else{
                $result_code = $result["code"];
            }

            $this->response( $result, $result_code );
        }
    }


}
