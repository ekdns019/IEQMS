<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_model extends MY_Model
{
    public function __construct()
    {
        //parent::__construct();
        $this->load->database() ;
        $this->infoObj = new stdClass;

        $this->load->model('common/Createkey_model','createkey_model') ;
        //모델

    }

    //관리자///////////////////////////////////////////////
    //관리자 리스트
    public function get_list_adm($data){
        $_wsql = "";    //초기화
        $_sql_limit = ""; //초기화

        //페이징 사용하면
		if($data["page_state"]=="on"){
			$_sql_limit = " limit ".($data["ps"]*($data["gp"]-1)).",".$data["ps"];
		}

        //검색
        if($data["search"]=="on"){
			$_wsql .="
				AND (T1.adm_id like '%".$data["seartext"]."%' or T1.adm_name like '%".$data["seartext"]."%' or T1.adm_email like '%".$data["seartext"]."%' or T1.adm_phone like '%".$data["seartext"]."%' or T1.p_name like '%".$data["seartext"]."%' or T1.ma_groupname like '%".$data["seartext"]."%' )
			";
		}

        //정렬
        if($data["ord"]<>""){
            if($data["ord_type"]==""){  $data["ord_type"] = "ASC";  }
            $_ord="	T1.".$data["ord"]." ".$data["ord_type"]." ";
        }else{
            $_ord="	T1.adm_code DESC ";
        }

        $_sql = "
            SELECT
                T1.*
            FROM
                (
                SELECT
    			   T1.*, T2.p_name, T3.ma_groupname
    			FROM
    				TBL_ADM AS T1,
                    TBL_POSITION AS T2,
                    TBL_MENUAUTH AS T3
                WHERE
                    T1.p_code=T2.p_code
                    AND T1.ma_code=T3.ma_code
                ) AS T1
            WHERE
                T1.adm_code<>''
                ".$_wsql."
            ORDER BY
    			".$_ord."
		";

        //전체
        $totalcnt =	"
    		SELECT COUNT(1) AS CTN
    		FROM
    		(
                ".$_sql."
    		) AS T1
		";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->row_array() ;
		$_data['total_cnt'] = $_res->num_rows()						;

        //list
        $_sql = $_sql.$_sql_limit;

        $_res = $this->db->query($_sql) ;
        $_data['data'] = $_res->result_array() ;

        return json_encode($_data) ;
    }

    //관리자 뷰
    public function get_view_adm($data){
        $_sql = "
        SELECT
            T1.adm_code, T1.adm_id, T1.adm_name, T1.adm_email, T1.adm_phone, T1.adm_wdate, T1.adm_udate, T1.adm_lastlogindate, T1.ma_code, T3.ma_groupname as ma_name, T1.p_code, T2.p_name
        FROM
        (
			SELECT
			   *
			FROM
				TBL_ADM
            WHERE
                adm_code='".$data["adm_code"]."'
        ) AS T1

        LEFT OUTER JOIN
        (
            SELECT p_code, p_name
            FROM TBL_POSITION
        ) AS T2 ON T1.p_code=T2.p_code

        LEFT OUTER JOIN
        (
            SELECT ma_code, ma_groupname
            FROM TBL_MENUAUTH
        ) AS T3 ON T1.ma_code=T3.ma_code
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //관리자 등록
    public function post_adm($data){
        $data["adm_code"] = $this->createkey_model->createKeyN("TBL_ADM", "adm_code","")   ; //키생성
        if($data["adm_code"]==""){
            $_result["code"] = 500;
            $_result["msg"] = "관리자 등록시 키생성 실패";
            $_result["type"] = "false";
            $_result["data"] = array();
            return json_encode($_result);
            exit;
        }

        $_sql = "
			INSERT TBL_ADM
            SET
                adm_code='".$data["adm_code"]."'
                , adm_id='".$data["adm_id"]."'
                , adm_name='".$data["adm_name"]."'
                , adm_pwd=HEX(AES_ENCRYPT('".$data["adm_pwd"]."', SHA2('".BANK."',512)))
                , adm_email='".$data["adm_email"]."'
                , adm_phone='".$data["adm_phone"]."'
                , adm_wdate=now()
                , adm_udate=now()
                , adm_lastlogindate=now()
                , ma_code='".$data["ma_code"]."'
                , p_code='".$data["p_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "관리자 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 관리자 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //관리자 수정
    public function put_adm($data){
        $_usql="" ; //초기화
        if(trim($data["adm_pwd_change"])<>""){
            $_usql=", adm_pwd=HEX(AES_ENCRYPT('".$data["adm_pwd_change"]."', SHA2('".BANK."',512)))";
        }

        $_sql = "
			UPDATE TBL_ADM
            SET
                adm_name='".$data["adm_name"]."'
                , adm_email='".$data["adm_email"]."'
                , adm_phone='".$data["adm_phone"]."'
                , adm_udate=now()
                , ma_code='".$data["ma_code"]."'
                , p_code='".$data["p_code"]."'
                ".$_usql."
            WHERE
                adm_code='".$data["adm_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "관리자 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 관리자 수정 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //관리자 삭제
    public function delete_adm($data){
        $_sql = "
			DELETE FROM TBL_ADM
            WHERE
                adm_code ='".$data["adm_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "관리자 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 관리자 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //관리자///////////////////////////////////////////////
    //관리자 로그
    public function get_list_academylog2($data){

        //페이징 사용하면
		if($data["page_state"]=="on"){
			$_sql_limit = " limit ".($data["ps"]*($data["gp"]-1)).",".$data["ps"];
		}else{
			$_sql_limit = "";
		}

        //검색
        if($data["search"]=="on"){
			$_wsql .="
				AND (T1.adm_id like '%".$data["seartext"]."%' or T1.adm_code like '%".$data["seartext"]."%' or T1.al_msg = '".$data["seartext"]."' )
			";
		}

        //정렬
        if($data["ord"]<>""){
            if($data[ord_type]==""){  $data[ord_type] = "ASC";  }

            $_ord="	T1.".$data["ord"]." ".$data[ord_type]." ";
        }else{
            $_ord="	T1.al_code DESC ";
        }

        $_sql = "
            SELECT
                T1.*
            FROM
                (
                SELECT
    			   T1.*, T2.adm_id
    			FROM
    				TBL_ACADEMY_LOG2 AS T1,
                    TBL_ADM AS T2
                WHERE
                    T1.adm_code=T2.adm_code
                ) AS T1
            WHERE
                T1.al_code<>''
                ".$_wsql."
            ORDER BY
    			".$_ord."
		";

        //전체
        $totalcnt =	"
    		SELECT COUNT(1) AS CTN
    		FROM
    		(
                ".$_sql."
    		) AS T1
		";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->row_array() ;
		$_row['total_cnt'] = $_res->num_rows()						;

        //list
        $_sql = $_sql.$_sql_limit;
        $_res = $this->db->query($_sql) ;
        $_row['data'] = $_res->result_array() ;

        return json_encode($_row) ;
    }


    //관리자 로그 등록
    public function post_academylog2($data){
        //체크 필
        $_sql = "
			INSERT TBL_ADM
            SET
                adm_code ='".$data["adm_code"]."'
                , al_type='".$data["al_type"]."'
                , al_wdate=now()
                , al_msg='".$data["al_msg"]."'
                , a_date=now()
                , a_udate=now()
		";

        $result = $this->db->query($_sql);

        //로그기록은 따로 리턴값 없음.
        /*
        if($result==true){
			$_result["result_code"] = 200;
			$_result["result_msg"] = "관리자 등록 성공";
			$_result["result_type"] = "success";
			$_result["result_data"] = array();
		}else{
            $_result["result_code"] = 500;
			$_result["result_msg"] = "DB 오류 관리자 등록 실패";
			$_result["result_type"] = "false";
			$_result["result_data"] = array();
		}
		return json_encode($_result);
        */
    }


    //관리자_메뉴권한///////////////////////////////////////////////
    //메뉴권한 리스트
    public function get_list_menuauth($data){

        //페이징 사용하면
		if($data["page_state"]=="on"){
			$_sql_limit = " limit ".($data["ps"]*($data["gp"]-1)).",".$data["ps"];
		}else{
			$_sql_limit = "";
		}

        //검색
        if($data["search"]=="on"){
			$_wsql .="
				AND T1.ma_groupname like '%".$data["seartext"]."%'
			";
		}

        //정렬
        if($data["ord"]<>""){
            if($data[ord_type]==""){  $data[ord_type] = "ASC";  }

            $_ord="	T1.".$data["ord"]." ".$data[ord_type]." ";
        }else{
            $_ord="	T1.ma_code DESC ";
        }

        $_sql = "
            SELECT
			  ma_code, ma_groupname, ma_admin_auth
			FROM
				{$this->this_db}.TBL_MENUAUTH
            WHERE
                ma_code<>''
                ".$_wsql."
            ORDER BY
    			".$_ord."
		";

        //전체
        $totalcnt =	"
    		SELECT COUNT(1) AS CTN
    		FROM
    		(
                ".$_sql."
    		) AS T1
		";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->row_array() ;
		$_row['total_cnt'] = $_res->num_rows()						;

        //list
        $_sql = $_sql.$_sql_limit;
        $_res = $this->m_db->query($_sql) ;
        $_row['data'] = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //메뉴권한 뷰
    public function get_view_menuauth($data){
        $_sql = "
            SELECT
              ma_code, ma_groupname, ma_admin_auth
            FROM
                {$this->this_db}.TBL_MENUAUTH
            WHERE
                ma_code='".$data["ma_code"]."'
		";
        $_res = $this->m_db->query($_sql) ;
        $_row = $_res->result_array() ;
        $_row[0]["ma_admin_auth"] =  json_decode($_row[0]["ma_admin_auth"],true);

        return json_encode($_row) ;
    }

    //메뉴권한 등록
    public function post_menuauth($data){
        $data["ma_code"] = fncCreateKey("TBL_MENUAUTH", "ma_code");    //키값생성

        for($i=0;$i<count($data["depth1_r"]);$i++){
            $ma_admin_auth["r"]["depth1_r"][$i]=$data["depth1_r"];
        }

        for($i=0;$i<count($data["depth2_r"]);$i++){
            $ma_admin_auth["r"]["depth2_r"][$i]=$data["depth2_r"];
        }

        for($i=0;$i<count($data["depth3_r"]);$i++){
            $ma_admin_auth["r"]["depth3_r"][$i]=$data["depth3_r"];
        }


        for($i=0;$i<count($data["depth1_w"]);$i++){
            $ma_admin_auth["w"]["depth1_w"][$i]=$data["depth1_w"];
        }

        for($i=0;$i<count($data["depth2_w"]);$i++){
            $ma_admin_auth["w"]["depth2_w"][$i]=$data["depth2_w"];
        }

        for($i=0;$i<count($data["depth3_w"]);$i++){
            $ma_admin_auth["w"]["depth3_w"][$i]=$data["depth3_w"];
        }

        $ma_admin_auth_json = json_encode($ma_admin_auth);

        $_sql = "
			INSERT {$this->this_db}.TBL_MENUAUTH
            SET
                ma_code='".$data["ma_code"]."'
                , ma_groupname='".$data["ma_groupname"]."'
                , ma_admin_auth='".$ma_admin_auth_json."'
		";

        $result = $this->m_db->query($_sql);

        if($result==true){
			$_result["result_code"] = 200;
			$_result["result_msg"] = "메뉴권한 등록 성공";
			$_result["result_type"] = "success";
			$_result["result_data"] = array();
		}else{
            $_result["result_code"] = 500;
			$_result["result_msg"] = "DB 오류 메뉴권한 등록 실패";
			$_result["result_type"] = "false";
			$_result["result_data"] = array();
		}
		return json_encode($_result);
    }

    //메뉴권한 수정
    public function put_menuauth($data){

        for($i=0;$i<count($data["depth1_r"]);$i++){
            $ma_admin_auth["r"]["depth1_r"][$i]=$data["depth1_r"];
        }

        for($i=0;$i<count($data["depth2_r"]);$i++){
            $ma_admin_auth["r"]["depth2_r"][$i]=$data["depth2_r"];
        }

        for($i=0;$i<count($data["depth3_r"]);$i++){
            $ma_admin_auth["r"]["depth3_r"][$i]=$data["depth3_r"];
        }


        for($i=0;$i<count($data["depth1_w"]);$i++){
            $ma_admin_auth["w"]["depth1_w"][$i]=$data["depth1_w"];
        }

        for($i=0;$i<count($data["depth2_w"]);$i++){
            $ma_admin_auth["w"]["depth2_w"][$i]=$data["depth2_w"];
        }

        for($i=0;$i<count($data["depth3_w"]);$i++){
            $ma_admin_auth["w"]["depth3_w"][$i]=$data["depth3_w"];
        }

        $ma_admin_auth_json = json_encode($ma_admin_auth);

        $_sql = "
			UPDATE {$this->this_db}.TBL_MENUAUTH
            SET
                ma_groupname='".$data["ma_groupname"]."'
                , ma_admin_auth='".$ma_admin_auth_json."'
            WHERE
                ma_code='".$data["ma_code"]."'
		";

        $result = $this->m_db->query($_sql);

        if($result==true){
			$_result["result_code"] = 200;
			$_result["result_msg"] = "메뉴권한 수정 성공";
			$_result["result_type"] = "success";
			$_result["result_data"] = array();
		}else{
            $_result["result_code"] = 500;
			$_result["result_msg"] = "DB오류 메뉴권한 수정 실패 ";
			$_result["result_type"] = "false";
			$_result["result_data"] = array();
		}
		return json_encode($_result);
    }

    //메뉴권한 삭제
    public function delete_menuauth($data){
        $stateYN =fncStateCheck("TBL_ADM", "ma_code",$data["ma_code"]);

        if($stateYN["result"]==true){
            $_sql = "
    			DELETE FROM {$this->this_db}.TBL_MENUAUTH
                WHERE
                    ma_code ='".$data["ma_code"]."'
    		";

            $result = $this->m_db->query($_sql);

            if($result==true){
    			$_result["result_code"] = 200;
    			$_result["result_msg"] = "메뉴권한 삭제 성공";
    			$_result["result_type"] = "success";
    			$_result["result_data"] = array();
    		}else{
                $_result["result_code"] = 500;
    			$_result["result_msg"] = "DB오류 메뉴권한 삭제 실패 ";
    			$_result["result_type"] = "false";
    			$_result["result_data"] = array();
    		}
    		return json_encode($_result);

        }else{
            $_result["result_code"] = 550;
            $_result["result_msg"] = $stateYN["msg"];
            $_result["result_type"] = "false";
            $_result["result_data"] = array();
        }
    }


    //관리자_아이디중복체크///////////////////////////////////////////////
    //아이디 중복체크
    public function get_check_adminid($data){
        $_sql = "
            SELECT
              count(adm_code) as cnt
            FROM
                {$this->this_db}.TBL_ADM
            WHERE
                adm_id='".$data["adm_id"]."'
		";
        $_res = $this->m_db->query($_sql) ;
        $_row = $_res->result_array() ;

        if($_row[0]["cnt"]==0){
            $_result["result"] = true;
            $_result["msg"] = "중복된 아이디가 없습니다.";
        }else{
            $_result["result"] = true;
            $_result["msg"] = "아이디를 사용중입니다.";
        }

        return json_encode($_row) ;
    }

}
