<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends MY_Model
{
    public function __construct()
    {
        //parent::__construct();
        $this->load->database() ;
        $this->infoObj = new stdClass;

        $this->load->model('common/Createkey_model','createkey_model') ;
        //모델

    }

    /*
    //컨테츠관리
    $auth["content"]["all"]="rw";  //전체
    $auth["content"]["question"]="rw";  //문제등록 및 관리
    $auth["content"]["pape"]="rw";	 //내신기출, 모의수능사관기출및 PSET 시험지
    $auth["content"]["concept"]="rw";   //개념관리
    $auth["content"]["unit"]="rw";      //단원유형
    $auth["content"]["type"]="rw";      //학년 및시험분류
    $auth["content"]["qr"]="rw";        //QR코드

    //통합
    $auth["integrated"]["all"]="rw";  //전체
    $auth["integrated"]["school"]="rw";  //학교관리
    $auth["integrated"]["academy"]="rw"; //회원관리
    $auth["integrated"]["auth"]="rw";    //권한설정
    $auth["integrated"]["admin"]="rw";   //운영자관리

    //시험관리
    $auth["exam"]["all"]="rw";		 //시험지생성
    $auth["exam"]["create"]="rw";		 //시험지생성
    $auth["exam"]["scoreclinic"]="rw";   //성적처리 및 클리닉

    //고객지원관리
    $auth["customer"]["all"]="rw";		//문제오류접수
    $auth["customer"]["error"]="rw";		//문제오류접수
    $auth["customer"]["contentlist"]="rw";  //컨텐츠 등록내역
    $auth["customer"]["onenone"]="rw";		//1:1상담내역

    //D-test
    $auth["dtest"]["all"]="rw";		         //잔체
    $auth["dtest"]["mri"]="rw";				//mri
    $auth["dtest"]["mct"]="rw";				//mct
    $auth["dtest"]["pset"]="rw";			//pset
    //echo json_encode($auth);
    */

    //관리자_체크///////////////////////////////////////////////
    public function get_state_adm($data){
        //초기화
        $_result["code"] = 511;
        $_result["result"] = false;
        $_result["type"] = "danger";
        $_result["msg"] = "ID 호출 접근권한 없음";

        $_sql="
        SELECT
            T1.adm_code, IFNULL(T2.ma_admin_auth,'') as adm_auth
        FROM
        (
            SELECT
                adm_code, ma_code, adm_state
            FROM
                TBL_ADM
            WHERE
                adm_code='".$data["user_code"]."'
                AND adm_state='Y'
        ) AS T1
        LEFT OUTER JOIN
        (
            SELECT
                ma_code, ma_admin_auth
            FROM
                TBL_MENUAUTH
        ) AS T2 ON T1.ma_code=T2.ma_code
        ";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->row_array() ;
        $_total_cnt = $_res->num_rows()						;

        if($_total_cnt==1){
            if($_row["adm_auth"]<>""){

                //$data["_auth_name"][0] = 대메뉴;
                //$data["_auth_name"][1] = 중메뉴;
                //$data["_auth_name"][2] = r, w, rw;

                $_auth = json_decode($_row["adm_auth"],true);

                //대메뉴 있는지?
                if((array_key_exists($data["_auth_name"][0], $_auth))){
                    //대메뉴 전체 있는지
                    if((array_key_exists("all", $_auth[$data["_auth_name"][0]]))){

                    //전체권한에 r 권한 있는지?
                     if(strpos($_auth[$data["_auth_name"][0]]["all"],$data["_auth_name"][2])!==false){
                            $_result["code"] = 200;
                            $_result["result"] = true;
                            $_result["type"] = "success";
                            $_result["msg"] = "대메뉴 전체권한 허용";

                            return json_encode($_result) ;
                            exit;
                        }
                    }

                    //전체 권한에 해당 권한 없다면
                    if($_result["result"]==false){

                        //즁매뉴 있는지?
                        if((array_key_exists($data["_auth_name"][1], $_auth[$data["_auth_name"][0]]))){
                            //중메뉴의 해당 권한(r, w, rw) 있는지?
                            if(strpos($_auth[$data["_auth_name"][0]][$data["_auth_name"][1]],$data["_auth_name"][2])!==false){

                                $_result["code"] = 200;
                                $_result["result"] = true;
                                $_result["type"] = "success";
                                $_result["msg"] = "중메뉴 해당 권한 허용";

                                return json_encode($_result) ;
                                exit;

                            }

                        }

                    }

                }

            }

        }

        return json_encode($_result) ;
    }

    //관리자_메뉴 불러오기///////////////////////////////////////////////
    public function get_admmenu($adm_code){
        $_sql = "
            SELECT
                ma_admin_auth
            FROM
                TBL_ADM AS T1,
                TBL_MENUAUTH AS T2
            WHERE
                T1.adm_code='".$adm_code."'
                AND T1.ma_code=T2.ma_code
        ";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        if(trim($_row[0]["ma_admin_auth"])==""){
            $_result["code"] = 500;
            $_result["msg"] = "메뉴권한 없음";
            $_result["type"] = "danger";
            $_result["data"] = array();

        }else{
            $_result["code"] = 200;
            $_result["msg"] = "메뉴 불러오기";
            $_result["type"] = "success";
            $_result["data"] = json_decode($_row[0]["ma_admin_auth"],true);
        }

        return json_encode($_result);
    }

}
