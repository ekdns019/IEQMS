<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codemanage_model extends MY_Model
{
    public function __construct()
    {
        //parent::__construct();
        $this->load->database() ;
        $this->infoObj = new stdClass;

        $this->load->model('common/Createkey_model','createkey_model') ;
        //모델

    }

    //코드학제///////////////////////////////////////////////
    //코드학제 리스트
    public function get_list_codeschool($data){
        $_sql = "
			SELECT
			   *
			FROM
				TBL_CODE_SCHOOL
            ORDER BY
				cs_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드학제 뷰
    public function get_view_codeschool($data){
        $_sql = "
			SELECT
			   *
			FROM
				TBL_CODE_SCHOOL
            WHERE
                cs_code='".$data["cs_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드학제 등록
    public function post_codeschool($data){
        $data["cs_code"] = fncCreateKey("TBL_CODE_SCHOOL", "cs_code");    //키값생성
        $_sql = "
			INSERT TBL_CODE_SCHOOL
            SET
                cs_code='".$data["cs_code"]."'
                , cs_school='".$data["cs_school"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학제 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 학제 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학제 수정
    public function put_codeschool($data){
        $_sql = "
			UPDATE TBL_CODE_SCHOOL
            SET
                cs_school='".$data["cs_school"]."'
            WHERE
                cs_code='".$data["cs_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학제 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 학제 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학제 삭제
    public function delete_codeschool($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크
        //코드시험체크
        //코드과목체크
        //코드학년체크
        //교제제작_개념_컨텐츠
        //교재 출판관리
        //교재제작_문제
        //DTEST_레벨
        //DTEST_PSET_시험
        //OMR형식설정
        //카테고리
        //문항_검색정보
        //리포트클리닉
        //카테고리_사용자


        $_sql = "
			DELETE FROM TBL_CODE_SCHOOL
            WHERE
                cs_code='".$data["cs_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학제 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 학제 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }


    //코드학년///////////////////////////////////////////////
    //코드학년 리스트
    public function get_list_codeschoollevel($data){
        $_sql = "
			SELECT
			   T1.csl_code, T1.csl_levelname, T2.cs_code, T2.cs_school
			FROM
				TBL_CODE_SCHOOL_LEVEL AS T1,
                TBL_CODE_SCHOOL AS T2
            WHERE
                T1.cs_code=T2.cs_code
            ORDER BY
				T1.csl_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드학년 뷰
    public function get_view_codeschoollevel($data){
        $_sql = "
            SELECT
               T1.csl_code, T1.csl_levelname, T2.cs_code, T2.cs_school
            FROM
                TBL_CODE_SCHOOL_LEVEL AS T1,
                TBL_CODE_SCHOOL AS T2
            WHERE
                T1.cs_code=T2.cs_code
                AND T1.csl_code='".$data["csl_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드학년 등록
    public function post_codeschoollevel($data){
        $data["csl_code"] = $this->createkey_model->createKeyN("TBL_CODE_SCHOOL_LEVEL", "csl_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_SCHOOL_LEVEL
            SET
                csl_code='".$data["csl_code"]."'
                , cs_code='".$data["cs_code"]."'
                , csl_levelname='".$data["csl_levelname"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학년 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 학년 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학년 수정
    public function put_codeschoollevel($data){
        $_sql = "
			UPDATE TBL_CODE_SCHOOL_LEVEL
            SET
                cs_code='".$data["cs_code"]."'
                , csl_levelname='".$data["csl_levelname"]."'
            WHERE
                csl_code='".$data["csl_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학년 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 학년 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학년 삭제
    public function delete_codeschoollevel($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_SCHOOL_LEVEL
            WHERE
                csl_code='".$data["csl_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학년 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 학제 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학기///////////////////////////////////////////////
    //코드학기 리스트
    public function get_list_codeschoolseason($data){
        $_wsql = "";    //초기화
        //검색
        if($data["search"]<>""){
            $_wsql="
                AND (
                    T1.css_subject like '%".$data["search"]."%'
                    OR T1.csl_levelname like '%".$data["search"]."%'
                    OR T1.cs_school like '%".$data["search"]."%'
                )
            ";
        }

        //정렬
        if($data["ord"]<>""){
            if($data[ord_type]==""){  $data[ord_type] = "ASC";  }

            $_ord="
				T1.".$data["ord"]." ".$data[ord_type]."
            ";
        }else{
            $_ord="
				T1.css_code ASC
            ";
        }

        $_sql = "
        SELECT
            T1.css_code, T1.css_subject, T1.csl_code, T1.csl_levelname, T1.cs_code, T1.cs_school
        FROM
        (
        	SELECT
			   T1.css_code, T1.css_subject, T2.csl_code, T2.csl_levelname, T3.cs_code, T3.cs_school
			FROM
                TBL_CODE_SCHOOL_SEASON AS T1,
				TBL_CODE_SCHOOL_LEVEL AS T2,
                TBL_CODE_SCHOOL AS T3
            WHERE
                T1.csl_code=T2.csl_code
                AND T2.cs_code=T3.cs_code
        ) AS T1
        WHERE
            T1.css_code<>''
            ".$_wsql."
        ORDER BY
            ".$_ord."
		";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드학기 뷰
    public function get_view_codeschoolseason($data){

        $_sql = "
        	SELECT
			   T1.css_code, T1.css_subject, T2.csl_code, T2.csl_levelname, T3.cs_code, T3.cs_school
			FROM
                TBL_CODE_SCHOOL_SEASON AS T1,
				TBL_CODE_SCHOOL_LEVEL AS T2,
                TBL_CODE_SCHOOL AS T3
            WHERE
                T1.csl_code=T2.csl_code
                AND T2.cs_code=T3.cs_code
                AND css_code='".$data["css_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드학기 등록
    public function post_codeschoolseason($data){
        $data["css_code"] = $this->createkey_model->createKeyN("TBL_CODE_SCHOOL_SEASON", "css_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_SCHOOL_SEASON
            SET
                css_code='".$data["css_code"]."'
                , css_subject='".$data["css_subject"]."'
                , csl_code='".$data["csl_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학기 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 학기 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학기 수정
    public function put_codeschoolseason($data){
        $_sql = "
			UPDATE TBL_CODE_SCHOOL_SEASON
            SET
            css_subject='".$data["css_subject"]."'
            , csl_code='".$data["csl_code"]."'
            WHERE
                css_code='".$data["css_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학기 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 학기 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드학기 삭제
    public function delete_codeschoolseason($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_SCHOOL_SEASON
            WHERE
                css_code='".$data["css_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "학기 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 학기 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }


    //코드과목///////////////////////////////////////////////
    //코드과목 리스트
    public function get_list_codesubject($data){
        $_sql = "
			SELECT
			   T1.csj_code, T1.csj_subject, T2.cs_code, T2.cs_school
			FROM
				TBL_CODE_SUBJECT AS T1,
                TBL_CODE_SCHOOL AS T2
            WHERE
                T1.cs_code=T2.cs_code
            ORDER BY
				T1.csj_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드과목 뷰
    public function get_view_codesubject($data){
        $_sql = "
        SELECT
           T1.csj_code, T1.csj_subject, T2.cs_code, T2.cs_school
        FROM
            TBL_CODE_SUBJECT AS T1,
            TBL_CODE_SCHOOL AS T2
        WHERE
            T1.cs_code=T2.cs_code
            AND T1.csj_code='".$data["csj_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드과목 등록
    public function post_codesubject($data){
        $data["csj_code"] = $this->createkey_model->createKeyN("TBL_CODE_SUBJECT", "csj_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_SUBJECT
            SET
                csj_code='".$data["csj_code"]."'
                , cs_code='".$data["cs_code"]."'
                , csj_subject='".$data["csj_subject"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "과목 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 과목 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드과목 수정
    public function put_codesubject($data){
        $_sql = "
			UPDATE TBL_CODE_SUBJECT
            SET
                cs_code='".$data["cs_code"]."'
                , csj_subject='".$data["csj_subject"]."'
            WHERE
                csj_code='".$data["csj_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "과목 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 과목 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드과목 삭제
    public function delete_codesubject($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_SUBJECT
            WHERE
                csj_code='".$data["csj_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "과목 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 과목 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드계열///////////////////////////////////////////////
    //코드계열 리스트
    public function get_list_codedepartment($data){
        $_sql = "
			SELECT
			   cd_code, cd_subject
			FROM
				TBL_CODE_DEPARTMENT
            ORDER BY
				cd_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드계열 뷰
    public function get_view_codedepartment($data){
        $_sql = "
        SELECT
           cd_code, cd_subject
        FROM
            TBL_CODE_DEPARTMENT
        WHERE
            cd_code='".$data["cd_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드계열 등록
    public function post_codedepartment($data){
        $data["cd_code"] = $this->createkey_model->createKeyN("TBL_CODE_DEPARTMENT", "cd_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_DEPARTMENT
            SET
                cd_code='".$data["cd_code"]."'
                , cd_subject='".$data["cd_subject"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "계열 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 계열 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드계열 수정
    public function put_codedepartment($data){
        $_sql = "
			UPDATE TBL_CODE_DEPARTMENT
            SET
                cd_subject='".$data["cd_subject"]."'
            WHERE
                cd_code='".$data["cd_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "계열 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 과목 계열 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드계열 삭제
    public function delete_codedepartment($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_DEPARTMENT
            WHERE
                cd_code='".$data["cd_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "계열 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 계열 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }


    //코드기관///////////////////////////////////////////////
    //코드기관 리스트
    public function get_list_codeagency($data){
        $_sql = "
			SELECT
			   ca_code, ca_subject
			FROM
				TBL_CODE_AGENCY
            ORDER BY
				ca_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드기관 뷰
    public function get_view_codeagency($data){
        $_sql = "
        SELECT
            ca_code, ca_subject
        FROM
            TBL_CODE_AGENCY
        WHERE
            ca_code='".$data["ca_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드기관 등록
    public function post_codeagency($data){
        $data["ca_code"] = $this->createkey_model->createKeyN("TBL_CODE_AGENCY", "ca_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_AGENCY
            SET
                ca_code='".$data["ca_code"]."'
                , ca_subject='".$data["ca_subject"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "기관 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 기관 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드기관 수정
    public function put_codeagency($data){
        $_sql = "
			UPDATE TBL_CODE_AGENCY
            SET
                ca_subject='".$data["ca_subject"]."'
            WHERE
                ca_code='".$data["ca_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "기관 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 과목 기관 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드기관 삭제
    public function delete_codeagency($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_AGENCY
            WHERE
                ca_code='".$data["ca_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "기관 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 기관 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드배점///////////////////////////////////////////////
    //코드배점 리스트
    public function get_list_codescore($data){
        $_sql = "
			SELECT
			   csr_code, csr_subject, csr_score
			FROM
				TBL_CODE_SCORE
            ORDER BY
				csr_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드기관 뷰
    public function get_view_codescore($data){
        $_sql = "
        SELECT
             csr_code, csr_subject, csr_score
        FROM
            TBL_CODE_SCORE
        WHERE
            csr_code='".$data["csr_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드기관 등록
    public function post_codescore($data){
        $data["csr_code"] = $this->createkey_model->createKeyN("TBL_CODE_SCORE", "csr_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_SCORE
            SET
                csr_code='".$data["csr_code"]."'
                , csr_subject='".$data["csr_subject"]."'
                , csr_score='".$data["csr_score"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "배점 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 기관 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드기관 수정
    public function put_codescore($data){
        $_sql = "
			UPDATE TBL_CODE_SCORE
            SET
                csr_subject='".$data["csr_subject"]."'
                , csr_score='".$data["csr_score"]."'
            WHERE
                csr_code='".$data["csr_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "배점 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 배점 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드기관 삭제
    public function delete_codescore($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_SCORE
            WHERE
                csr_code='".$data["csr_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "배점 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 배점 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }


    //코드시험///////////////////////////////////////////////
    //코드시험 리스트
    public function get_list_codeexam($data){
        $_sql = "
			SELECT
			   T1.ce_code, T1.ce_subject, T2.cs_code, T2.cs_school
			FROM
				TBL_CODE_EXAM AS T1,
                TBL_CODE_SCHOOL AS T2
            WHERE
                T1.cs_code=T2.cs_code
            ORDER BY
				T1.ce_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드시험 뷰
    public function get_view_codeexam($data){
        $_sql = "
        SELECT
           T1.ce_code, T1.ce_subject, T2.cs_code, T2.cs_school
        FROM
            TBL_CODE_EXAM AS T1,
            TBL_CODE_SCHOOL AS T2
        WHERE
            T1.cs_code=T2.cs_code
            AND T1.ce_code='".$data["ce_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드시험 등록
    public function post_codeexam($data){
        $data["ce_code"] = $this->createkey_model->createKeyN("TBL_CODE_EXAM", "ce_code","")   ; //키생성
        $_sql = "
			INSERT TBL_CODE_EXAM
            SET
                ce_code='".$data["ce_code"]."'
                , cs_code='".$data["cs_code"]."'
                , ce_subject='".$data["ce_subject"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "시험 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 시험 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드시험 수정
    public function put_codeexam($data){
        $_sql = "
			UPDATE TBL_CODE_EXAM
            SET
                cs_code='".$data["cs_code"]."'
                , ce_subject='".$data["ce_subject"]."'
            WHERE
                ce_code='".$data["ce_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "시험 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 시험 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드시험 삭제
    public function delete_codeexam($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_CODE_EXAM
            WHERE
                ce_code='".$data["ce_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "시험 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 시험 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드평가항목일반///////////////////////////////////////////////
    //코드평가항목일반 리스트
    public function get_list_reportclinicevaluationnomal($data){
        $_sql = "
			SELECT
			   T1.rcen_code, T1.rce_subject, T2.cs_code, T2.cs_school
			FROM
				TBL_REPORT_CLINIC_EVALUATION_NORMAL AS T1,
                TBL_CODE_SCHOOL AS T2
            WHERE
                T1.cs_code=T2.cs_code
            ORDER BY
				rcen_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드평가항목일반 뷰
    public function get_view_reportclinicevaluationnomal($data){
        $_sql = "
        SELECT
           T1.rcen_code, T1.rce_subject, T2.cs_code, T2.cs_school
        FROM
            TBL_REPORT_CLINIC_EVALUATION_NORMAL AS T1,
            TBL_CODE_SCHOOL AS T2
        WHERE
            T1.cs_code=T2.cs_code
            AND rcen_code='".$data["rcen_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드평가항목일반 등록
    public function post_reportclinicevaluationnomal($data){
        $data["rcen_code"] = $this->createkey_model->createKeyN("TBL_REPORT_CLINIC_EVALUATION_NORMAL", "rcen_code","")   ; //키생성
        $_sql = "
			INSERT TBL_REPORT_CLINIC_EVALUATION_NORMAL
            SET
                rcen_code='".$data["rcen_code"]."'
                , cs_code='".$data["cs_code"]."'
                , rce_subject='".$data["rce_subject"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "리포트클리닉평가항목_일반 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 리포트클리닉평가항목_일반 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드평가항목일반 수정
    public function put_reportclinicevaluationnomal($data){
        $_sql = "
			UPDATE TBL_REPORT_CLINIC_EVALUATION_NORMAL
            SET
                cs_code='".$data["cs_code"]."'
                , rce_subject='".$data["rce_subject"]."'
            WHERE
                rcen_code='".$data["rcen_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "리포트클리닉평가항목_일반 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 리포트클리닉평가항목_일반 수정  실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드평가항목일반 삭제
    public function delete_reportclinicevaluationnomal($data){

        //삭제전 해당 코드가 다른데 쓰는지 체크

        $_sql = "
			DELETE FROM TBL_REPORT_CLINIC_EVALUATION_NORMAL
            WHERE
                rcen_code='".$data["rcen_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "리포트클리닉평가항목_일반 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 리포트클리닉평가항목_일반 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드직급///////////////////////////////////////////////
    //코드직급 리스트
    public function get_list_position($data){
        $_sql = "
			SELECT
			   *
			FROM
				TBL_POSITION
            ORDER BY
				p_code ASC
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드직급 뷰
    public function get_view_position($data){
        $_sql = "
			SELECT
			   *
			FROM
				TBL_POSITION
            WHERE
                p_code='".$data["p_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        return json_encode($_row) ;
    }

    //코드직급 등록
    public function post_position($data){
        $data["p_code"] = $this->createkey_model->createKeyN("TBL_POSITION", "p_code","")   ; //키생성
        $_sql = "
			INSERT TBL_POSITION
            SET
                p_code='".$data["p_code"]."'
                , p_name='".$data["p_name"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "관리자 직급 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 관리자 직급 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드직급 수정
    public function put_position($data){
        $_sql = "
			UPDATE TBL_POSITION
            SET
                p_name='".$data["p_name"]."'
            WHERE
                p_code='".$data["p_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "관리자 직급 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 관리자 직급 수정 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //코드직급 삭제
    public function delete_position($data){

        $_sql = "
			DELETE FROM TBL_POSITION
            WHERE
                p_code='".$data["p_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "관리자 직급 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 관리자 직급 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

}
