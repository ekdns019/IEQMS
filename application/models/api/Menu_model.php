<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends MY_Model
{
    public function __construct()
    {
        //parent::__construct();
        $this->load->database() ;
        $this->infoObj = new stdClass;

        $this->load->model('common/Createkey_model','createkey_model') ;
        //모델

    }

    //메뉴권한///////////////////////////////////////////////
    //메뉴권한 리스트
    public function get_list_menuauth($data){
        $_wsql = "";    //초기화
        $_sql_limit = ""; //초기화

        //페이징 사용하면
		if($data["page_state"]=="on"){
			$_sql_limit = " limit ".($data["ps"]*($data["gp"]-1)).",".$data["ps"];
		}

        //검색
        if($data["search"]=="on"){
			$_wsql .="
				AND (adm_id like '%".$data["seartext"]."%' or adm_name like '%".$data["seartext"]."%' or adm_email like '%".$data["seartext"]."%' or adm_phone like '%".$data["seartext"]."%' or p_name like '%".$data["seartext"]."%' or ma_groupname like '%".$data["seartext"]."%' )
			";
		}

        //정렬
        if($data["ord"]<>""){
            if($data["ord_type"]==""){  $data["ord_type"] = "ASC";  }
            $_ord="	".$data["ord"]." ".$data["ord_type"]." ";
        }else{
            $_ord="	ma_code DESC ";
        }

        $_sql = "

            SELECT
			   ma_code, ma_groupname
			FROM
				TBL_MENUAUTH
            WHERE
                ma_code<>''
                ".$_wsql."
            ORDER BY
    			".$_ord."
		";

        //전체
        $totalcnt =	"
    		SELECT COUNT(1) AS CTN
    		FROM
    		(
                ".$_sql."
    		) AS T1
		";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->row_array() ;
		$_data['total_cnt'] = $_res->num_rows()						;

        //list
        $_sql = $_sql.$_sql_limit;

        $_res = $this->db->query($_sql) ;
        $_data['data'] = $_res->result_array() ;

        return json_encode($_data) ;
    }

    //메뉴권한 뷰
    public function get_view_menuauth($data){
        $_sql = "
		SELECT
		   ma_code, ma_groupname, ma_admin_auth
		FROM
			TBL_MENUAUTH
        WHERE
            ma_code='".$data["ma_code"]."'
		";
        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        $_row[0]["ma_admin_auth"] = json_decode($_row[0]["ma_admin_auth"],true);

        return json_encode($_row) ;
    }

    //메뉴권한 등록
    public function post_menuauth($data){
        $data["ma_code"] = $this->createkey_model->createKeyN("TBL_MENUAUTH", "ma_code","")   ; //키생성
        if($data["ma_code"]==""){
            $_result["code"] = 500;
            $_result["msg"] = "메뉴권한 등록시 키생성 실패";
            $_result["type"] = "false";
            $_result["data"] = array();
            return json_encode($_result);
            exit;
        }

        $_sql = "
			INSERT TBL_MENUAUTH
            SET
                ma_code='".$data["ma_code"]."'
                , ma_groupname='".$data["ma_groupname"]."'
                , ma_admin_auth=''
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "메뉴권한 등록 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 메뉴권한 등록 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //메뉴권한 수정
    public function put_menuauth($data){

        $_sql = "
			UPDATE TBL_MENUAUTH
            SET
                ma_groupname='".$data["ma_groupname"]."'
                , ma_admin_auth='".json_encode($data["ma_admin_auth"])."'
            WHERE
                ma_code='".$data["ma_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "메뉴권한 수정 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB 오류 메뉴권한 수정 실패";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

    //메뉴권한 삭제
    public function delete_menuauth($data){
        $_sql = "
			DELETE FROM TBL_MENUAUTH
            WHERE
                ma_code ='".$data["ma_code"]."'
		";

        $result = $this->db->query($_sql);

        if($result==true){
			$_result["code"] = 200;
			$_result["msg"] = "메뉴권한 삭제 성공";
			$_result["type"] = "success";
			$_result["data"] = array();
		}else{
            $_result["code"] = 500;
			$_result["msg"] = "DB오류 메뉴권한 삭제 실패 ";
			$_result["type"] = "false";
			$_result["data"] = array();
		}
		return json_encode($_result);
    }

}
