<?php
defined('BASEPATH') OR exit('No direct script access allowed') ;

class Createkey_model extends MY_Model
{
    public function __construct(){

        //parent::__construct()   ;
        $this->load->database() ;
        $this->searchObj = new stdClass() ;

        $this->load->helper('common');
    }

    // 출결 키 생성
    public function createKeyN($TABLE, $key_field, $target_db){
        $txt="";
        switch ($TABLE) {
    	//코드_학제
    	case "TBL_CODE_SCHOOL":
    		$txt = "CS0";
    		break;
    	//코드_학년
    	case "TBL_CODE_SCHOOL_LEVEL":
    		$txt = "CSL";
    		break;
    	//코드_학기
    	case "TBL_CODE_SCHOOL_SEASON":
    		$txt = "CSS";
    		break;
    	//코드_과목
    	case "TBL_CODE_SUBJECT":
    		$txt = "CSJ";
    		break;
    	//코드_시험
    	case "TBL_CODE_EXAM":
    		$txt = "CE0";
    		break;
    	//코드_출판사
    	case "TBL_CODE_PUBLISHER":
    		$txt = "CP0";
    		break;
    	//교제출판관리
    	case "TBL_BOOK_INFO":
    		$txt = "B00";
    		break;
    	//코드_계열
    	case "TBL_CODE_DEPARTMENT":
    		$txt = "CD0";
    		break;
    	//학교명
    	case "TBL_CODE_SCHOOLNAME":
    		$txt = "CSN";
    		break;
    	//코드_기관
    	case "TBL_CODE_AGENCY":
    		$txt = "CA0";
    		break;
    	//코드_배점
    	case "TBL_CODE_SCORE":
    		$txt = "CSR";
    		break;
    	//관리자_로그인
    	case "TBL_ADM":
    		$txt = "ADM";
    		break;
    	//관리자_직급
    	case "TBL_POSITION":
    		$txt = "P00";
    		break;
    	//관리자_메뉴권한
    	case "TBL_MENUAUTH":
    		$txt = "MA0";
    		break;
    	//관리자로그
    	case "TBL_ACADEMY_LOG2":
    		$txt = "AL0";
    		break;
    	//문항_출처
    	case "TBL_CODE_QSOURCE_GROUP":
    		$txt = "EQG";
    		break;
    	//문항_검색정보
    	case "TBL_QSOURCE_DETAIL":
    		$txt = "QD0";
    		break;
    	//카테고리
    	case "TBL_MAIN_CATEGORY":
    		$txt = "MC0";
    		break;
    	//해시태그
    	case "TBL_CLASS_GROUP":
    		$txt = "HT0";
    		break;
    	//리포트클리닉평가항목_일반
    	case "TBL_REPORT_CLINIC_EVALUATION_NORMAL":
    		$txt = "RCE";
    		break;
    	//리포트클리닉
    	case "TBL_REPORT_CLINIC":
    		$txt = "RCL";
    		break;
    	}

        if($txt==""){
    		return "";
    	}else{

    		if($txt=="n"){ $txt=""; }

    		//TXTYYMMDD_000000(문자(3),년(2),월(2),일(2),구분자(1),숫자(6)  총 16
    		$txt.=substr(date("Y"),2,2).date("md")."_".date("His");

    		$_sql="select ".$key_field." AS last_code from ".$TABLE." where ".$key_field." like '".$txt."%' order by ".$key_field." desc limit 0, 1";

    		$_res = $this->db->query($_sql) ;
            $_row = $_res->result_array() ;

            if($_row==null){
                $new_key=$txt."0";
            }else{

                $key=$_row[0]["last_code"];
                $key_arr=substr($key,strlen($key-1),1);
    			$keynumber=(int)$key_arr;
    			$keynumber++;
                //혹시라도 동시 클릭으로 인한 중복시 1씩증가  증복처리 9가 넘어가면 그냥 오류로 처리
                if(strlen($keynumber)==2){
                    return "";
                }

    			$new_key=$txt.$keynumber;
            }

            return $new_key;
    	}
    }

    // 태이블내 해당 키값 있는지 체크
    public function statecheck($TABLE, $key_code, $key_value){

        $_sql="select count(".$key_code.") AS cnt from ".$TABLE." where ".$key_code." = '".$key_value."'";

        $_res = $this->db->query($_sql) ;
        $_row = $_res->result_array() ;

        //데이터가 있으면 Flase, 없고 등록가능하면 True
        if($_row[0]["cnt"]>0){
            $_return["code"]  = "200";
            $_return["result"]  = false;
            $_return["msg"]  = "해당 데이터가 사용중 입니다.";
        }else{
            $_return["code"]  = "560";
            $_return["result"]  = true;
            $_return["msg"]  = "등록가능";
        }

        return $_return;

    }
}
