<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





//데이터 블랭크 표시
if ( ! function_exists('fncBlank')){
	function fncBlank($data=null) {
		if(trim($data)==""){
			$data_value="-";
		}else{
			$data_value=$data;
		}
		return $data_value;
	}
}

//미기입 표시
if ( ! function_exists('fncBlankText')){
	function fncBlankText($data=null) {
		if(trim($data)==""){
			$data_value="<span style='color:#b6b6b6'>미기입</span>";
		}else{
			$data_value=$data;
		}
		return $data_value;
	}
}

//디데이 D-day
if ( ! function_exists('fncDday')){
	function fncDday($date,$now = 'now',$format = '%+d',$str_dday = ''){
		$dday = floor(( strtotime(substr($date,0,10)) - strtotime($now) )/86400 * -1 );
		return $dday != 0 ? sprintf($format,$dday) : $str_dday;
	}
}

//파일업로드
if ( ! function_exists('fncFileUpload')){
	function fncFileUpload($upfolder, $BbsFile, $BbsFile_name, $BbsFile_size, $BbsFile_type) {
		$upFile = $BbsFile;

		If (!Is_Null($upFile)) {
			$FileSize	= $BbsFile_size;
			$FileName	= $BbsFile_name;
			$FileType	= $BbsFile_type;


			//20메가
			If ($FileSize > 20480000){
				fncMsg("첨부파일의 용량은 20MB로 제한되어있습니다.","");
			}


			If ($FileSize > 0) {

				//FileNameDivide = Split(FileName,".")

				$FileName1 = substr($FileName,0,strlen($FileName)-4);
				$FileName2 = substr($FileName, strlen($FileName)-3);
				if($FileName2<>"doc" and $FileName2<>"ppt" and $FileName2<>"jpg" and $FileName2<>"gif" and $FileName2<>"bmp" and $FileName2<>"hwp" and $FileName2<>"pdf" and $FileName2<>"docx" and $FileName2<>"pptx" and $FileName2<>"xls" and $FileName2<>"xlsx" and $FileName2<>"ocx" and $FileName2<>"ptx" and $FileName2<>"lsx" and $FileName2<>"png" and $FileName2<>"peg" and $FileName2<>"zip"){
					fncMsg('이미지, 문서만 첨부가능합니다.','');
					exit;
				}
				$FileExist = True;
				$FileIDX = 0;
				do {
					If (is_file($upfolder.$FileName)) {
						$FileIDX = $FileIDX + 1;
						$FileName = $FileName1."_".$FileIDX.".".$FileName2;
					} Else {
						$FileExist = False;
					}
				} While ($FileExist);

				//fncMsg ($upfolder.$FileName);
				//echo $upFile."___________".$upfolder.$FileName."<br>";
				@move_uploaded_file($upFile, $upfolder.$FileName);

				$fncFileUpload["result"] = true;
				//$fncFileUpload["file_name"] = iconv("EUC-KR","UTF-8", $FileName);
				$fncFileUpload["file_name"] = iconv("EUC-KR","UTF-8", $FileName);
					
			} Else {
				$fncFileUpload["result"] = False;
				$fncFileUpload["file_name"] = "";
			}

		} Else {
			$fncFileUpload["result"] = False;
			$fncFileUpload["file_name"] = "";
		}

        return json_encode($fncFileUpload);
	}
}

//파일업로드
if ( ! function_exists('fncFileUpload_my')){
	function fncFileUpload_my($upfolder, $BbsFile, $BbsFile_name, $BbsFile_size, $BbsFile_type) {
		$upFile = $BbsFile;

		If (!Is_Null($upFile)) {
			$FileSize	= $BbsFile_size;
			$FileName	= $BbsFile_name;
			$FileType	= $BbsFile_type;


			//20메가
			If ($FileSize > 20480000){
				fncMsg("첨부파일의 용량은 20MB로 제한되어있습니다.","");
			}


			If ($FileSize > 0) {

				//FileNameDivide = Split(FileName,".")

				$FileName1 = substr($FileName,0,strlen($FileName)-4);
				$FileName2 = substr($FileName, strlen($FileName)-3);
				if($FileName2<>"doc" and $FileName2<>"ppt" and $FileName2<>"jpg" and $FileName2<>"gif" and $FileName2<>"bmp" and $FileName2<>"hwp" and $FileName2<>"pdf" and $FileName2<>"docx" and $FileName2<>"pptx" and $FileName2<>"xls" and $FileName2<>"xlsx" and $FileName2<>"ocx" and $FileName2<>"ptx" and $FileName2<>"lsx" and $FileName2<>"png" and $FileName2<>"peg" and $FileName2<>"zip"){
					fncMsg('이미지, 문서만 첨부가능합니다.','');
					exit;
				}

				//fncMsg ($upfolder.$FileName);
				//echo $upFile."___________".$upfolder.$FileName."<br>";
				@move_uploaded_file($upFile, $upfolder.$FileName);

				$fncFileUpload["result"] = true;
				//$fncFileUpload["file_name"] = iconv("EUC-KR","UTF-8", $FileName);
				$fncFileUpload["file_name"] = iconv("EUC-KR","UTF-8", $FileName);
					
			} Else {
				$fncFileUpload["result"] = False;
				$fncFileUpload["file_name"] = "";
			}

		} Else {
			$fncFileUpload["result"] = False;
			$fncFileUpload["file_name"] = "";
		}

        return json_encode($fncFileUpload);
	}
}

//파일업로드(같은이름 덥어씌우기 주로 파일명이 키값 혹은 코드처럼 유일한)
if ( ! function_exists('fncFileUpload_update')){
	Function fncFileUpload_update($upfolder,$BbsFile, $BbsFile_name, $BbsFile_size, $BbsFile_type) {
		$upFile = $BbsFile;


		If (!Is_Null($upFile)) {

			$FileSize	= $BbsFile_size;
			$FileName	= $BbsFile_name;
			$FileType	= $BbsFile_type;
			If ($FileSize > (1024000*2)){ 
				$result["result"] = false;
				$result["error_memo"]="첨부파일의 용량은 2MB로 제한되어있습니다.";

			}else{
				If ($FileSize > 0) {


					$FileName1 = substr($FileName,0,strlen($FileName)-4);
					$FileName2 = substr($FileName, strlen($FileName)-3);
					$FileName2_s=strtolower($FileName2);
					if($FileName2_s<>"doc" and $FileName2_s<>"ppt" and $FileName2_s<>"jpg" and $FileName2_s<>"gif" and $FileName2_s<>"bmp" and $FileName2_s<>"hwp" and $FileName2_s<>"pdf" and $FileName2_s<>"docx" and $FileName2_s<>"pptx" and $FileName2_s<>"xls" and $FileName2_s<>"xlsx" and $FileName2_s<>"ocx" and $FileName2_s<>"ptx" and $FileName2_s<>"lsx" and $FileName2_s<>"png" and $FileName2_s<>"peg"){
						$result["result"] = false;
						$result["error_memo"]="허용되는 확장자가 아닙니다.";
					}else{

						$FileExist = True;
						$fncFileUpload = iconv("EUC-KR","UTF-8", $FileName);
						@move_uploaded_file($upFile, $upfolder.$FileName);

						if($FileName2=='gif' || $FileName2=='GIF'){
							//썸네일 새성
							create_thumbnail($upfolder.$FileName, 1, 200, true);

						}elseif($FileName2=='jpg' || $FileName2=='JPG' || $FileName2=='JPEG' || $FileName2=='jpeg'){
							//썸네일 새성
							create_thumbnail($upfolder.$FileName, 2, 200, true);
						}
						$result["result"]		= true;
						$result["file_name"]	= $FileName;
						$result["error_memo"]	= "";
					}

				} Else {
					$result["result"] = false;
					$result["error_memo"]="파일용량오류";
				}
			}

		} Else {

			$result["result"] = false;
			$result["error_memo"]="파일이 없습니다.";
		}
		return $result;
	}
}



///////섬네일 이미지 생성
if ( ! function_exists('create_thumbnail')){
	function create_thumbnail($file, $sname, $max_side = false, $fixed = false) {
	// 1 = GIF, 2 = JPEG
	/*
	if ($sname == '1') {
		$sname = '_thumbnail';
	} else if ($sname == '2') {
		$sname = '_thumbnail2';
	}
	*/
	$sname = '_thumbnail';
	if(!$max_side){ $max_side = 125; }

	if(file_exists($file)) {
		$type = getimagesize($file);

		if(!function_exists('imagegif') && $type[2] == 1) {
			$error = 'Filetype not supported. Thumbnail not created.';
		} else if (!function_exists('imagejpeg') && $type[2] == 2) {
			$error = 'Filetype not supported. Thumbnail not created.';
		} else {
			// create the initial copy from the original file
			if($type[2] == 1) {
				$image = imagecreatefromgif($file);
			} else if($type[2] == 2) {
				$image = imagecreatefromjpeg($file);
			}

			if(function_exists('imageantialias')) imageantialias($image, TRUE);
			$image_attr = getimagesize($file);

			// figure out the longest side

				if($image_attr[0] > $image_attr[1]):
					$image_width = $image_attr[0];
					$image_height = $image_attr[1];

					if($fixed) {
						$image_new_width  = $max_side;
						$image_new_height = (int)($max_side * 3 / 4);
						// 4:3 ratio
					} else {
						$image_new_width = $max_side;

						$image_ratio = $image_width / $image_new_width;
						$image_new_height = (int) ($image_height / $image_ratio);
					}
					//width > height
				else:
					$image_width = $image_attr[0];
					$image_height = $image_attr[1];
					if($fixed) {
						$image_new_height = $max_side;
						$image_new_width  = (int)($max_side * 3 / 4);
						// 3:4 ratio
					} else {
						$image_new_height = $max_side;
						$image_ratio = $image_height / $image_new_height;
						$image_new_width = (int) ($image_width / $image_ratio);
					}
					//height > width
				endif;

				$thumbnail = imagecreatetruecolor($image_new_width, $image_new_height);
				@imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $image_new_width, $image_new_height, $image_attr[0], $image_attr[1]);
				$thumb = preg_replace('!(\.[^.]+)?$!', $sname.'$1', basename($file), 1);
				$thumbpath = str_replace(basename($file), $thumb, $file);

				// move the thumbnail to it's final destination
				if($type[2] == 1) {
					if (!imagegif($thumbnail, $thumbpath)) {
						$error = 'Thumbnail path invalid';
					}
				} else if($type[2] == 2) {
					if (!imagejpeg($thumbnail, $thumbpath)) {
						$error = 'Thumbnail path invalid';
					}
				}
			}
		} else {
			$error = 'File not found';
		}

		if(!empty($error)) {
			die($error);
		} else {
			return $thumbpath;
		}
	}
}


if ( ! function_exists('fncDeleteFilethumbnail')){
	function fncDeleteFilethumbnail($upfolder,$filename1) {
		$filename = substr($filename1,0,(strlen($filename1)-4))."_thumbnail".substr($filename1,-4);
		$filename2 = substr($filename1,0,(strlen($filename1)-4))."_thumbnail2".substr($filename1,-4);
		if (file_exists($upfolder.$filename)) {
			fncDeleteFile($upfolder.$filename);
		}
		if (file_exists($upfolder.$filename2)) {
			fncDeleteFile($upfolder.$filename2);
		}
	}
}



// 자바스크립의 ALERT 후 페이지 이동
if ( ! function_exists('fncMsg')){
	function fncMsg(){
		global $msg , $msg2 ;

		if (!func_get_args()){
			$msg = '오류메세지 : 메세지가 없습니다.\\n\\n관리자에게 문의하시기 바랍니다.';
		}
		$msg = func_get_arg(0);
		if (func_num_args() > 1){
			$msg2 = func_get_arg(1);
		}

		echo "<script type='text/JavaScript'>";
		if ($msg!=""){	echo "alert('".$msg."');";		}
		if ($msg2!=""){	echo "$msg2";		}
		echo "</script><noscript></noscript></body></html>";
		exit;
	}
}

//SMS 발송 알리고 API
if ( ! function_exists('fncAligoSMS')){
	function fncAligoSMS($recipients=null,$message=null,$ps_reservedte=null) {

		//echo "recipients=>".$recipients."<Br>";
		$sms_url = "https://apis.aligo.in/send/"; // 전송요청 URL
		$sms['user_id']	 = ds_id; // SMS 아이디
		$sms['key']		 = ds_key;//인증키
		$sms['sender']	 = ds_phone;			// 발신번호

		$sms['msg'] = stripslashes($message);
		$sms['receiver'] = $recipients;

		if($ps_reservedte<>""){
			$send_date=str_replace("-","",substr($ps_reservedte,0,10));
			$send_time=str_replace(":","",substr($ps_reservedte,11,5));

			$sms['rdate'] = $send_date;	//예약일자 - 20161004 : 2016-10-04일기준
			$sms['rtime'] = $send_time;	//예약시간 - 1930 : 오후 7시30분

		}
		$sms['testmode_yn'] = '';			// Y 인경우 실제문자 전송X , 자동취소(환불) 처리

		$host_info = explode("/", $sms_url);
		$port = $host_info[0] == 'https:' ? 443 : 80;

		$oCurl = curl_init();
		curl_setopt($oCurl, CURLOPT_PORT, $port);
		curl_setopt($oCurl, CURLOPT_URL, $sms_url);
		curl_setopt($oCurl, CURLOPT_POST, 1);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS, $sms);
		curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
		$ret = curl_exec($oCurl);
		curl_close($oCurl);

		$retArr = json_decode($ret); // 결과배열
		//echo "결과 배열값=>".print_r($retArr);

		if($retArr->result_code=="1"){
			$result["result"]=true;
			$result["message"]=$retArr->message;
			$result["success_cnt"]=$retArr->success_cnt;
			$result["error_cnt"]=$retArr->error_cnt;
		}else{
			$result["result"]=false;
			$result["message"]=$retArr->message;
			$result["success_cnt"]="";
			$result["error_cnt"]="";
		}

		return $result;
	}
}


//SMS 발송 알리고 API (강사용)
if ( ! function_exists('fncAligoSMSUser')){
	function fncAligoSMSUser($ds_id, $ds_key, $ds_phone, $recipients=null,$message=null,$ps_reservedte=null) {
		if($ds_id=="" || $ds_key=="" || $ds_phone==""){
			$result["result"]=false;
			$result["message"]="문자가입필요";
			$result["success_cnt"]="";
			$result["error_cnt"]="";
		}else{
			//echo "recipients=>".$recipients."<Br>";
			$sms_url = "https://apis.aligo.in/send/"; // 전송요청 URL
			$sms['user_id']	 = $ds_id; // SMS 아이디
			$sms['key']		 = $ds_key;//인증키
			$sms['sender']	 = $ds_phone;			// 발신번호


			$sms['msg'] = stripslashes($message);
			$sms['receiver'] = $recipients;
			if($ps_reservedte<>""){
				$send_date=str_replace("-","",substr($ps_reservedte,0,10));
				$send_time=str_replace(":","",substr($ps_reservedte,11,5));

				$sms['rdate'] = $send_date;	//예약일자 - 20161004 : 2016-10-04일기준
				$sms['rtime'] = $send_time;	//예약시간 - 1930 : 오후 7시30분

			}
			$sms['testmode_yn'] = '';			// Y 인경우 실제문자 전송X , 자동취소(환불) 처리

			$host_info = explode("/", $sms_url);
			$port = $host_info[0] == 'https:' ? 443 : 80;

			$oCurl = curl_init();
			curl_setopt($oCurl, CURLOPT_PORT, $port);
			curl_setopt($oCurl, CURLOPT_URL, $sms_url);
			curl_setopt($oCurl, CURLOPT_POST, 1);
			curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($oCurl, CURLOPT_POSTFIELDS, $sms);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			$ret = curl_exec($oCurl);
			curl_close($oCurl);

			$retArr = json_decode($ret); // 결과배열
//			echo "결과 배열값=>".print_r($retArr);

			if($retArr->result_code=="1"){
				$result["result"]=true;
				$result["message"]=$retArr->message;
                $result["msg_id"]=$retArr->msg_id;
				$result["success_cnt"]=$retArr->success_cnt;
				$result["error_cnt"]=$retArr->error_cnt;
			}else{
				$result["result"]=false;
//                $result["msg_id"]=$retArr->msg_id;
				$result["message"]=$retArr->message;
				$result["success_cnt"]="";
				$result["error_cnt"]="";
			}
		}

		return $result;
	}
}

//SMS 발송잔여건수 알리고 API (강사용)
if ( ! function_exists('fncAligoCountUser')){
	function fncAligoCountUser($ds_id, $ds_key) {
		/**************** 전송가능 건수확인 ******************/
		/* "result_code":결과코드,"message":결과문구, */
		/** SMS_CNT / LMS_CNT / MMS_CNT : 전송유형별 잔여건수 ***/
		/******************** 인증정보 ********************/
		$sms_url = "https://apis.aligo.in/remain/";		// 전송요청 URL
		$sms['user_id'] = $ds_id;						// SMS 아이디
		$sms['key'] = $ds_key;							// 인증키
		/******************** 인증정보 ********************/

		$host_info = explode("/", $sms_url);
		$port = $host_info[0] == 'https:' ? 443 : 80;

		$oCurl = curl_init();
		curl_setopt($oCurl, CURLOPT_PORT, $port);
		curl_setopt($oCurl, CURLOPT_URL, $sms_url);
		curl_setopt($oCurl, CURLOPT_POST, 1);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS, $sms);
		curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);

		// CURL 접속이 안되는 경우 CURL 옵션안내를 참조 하셔서 옵션을 출력하여 확인해 주세요.
		// http://phpdoc.me/manual/kr/function.curl-setopt.php
		$ret = curl_exec($oCurl);
		curl_close($oCurl);
		//echo $ret;
		$retArr = json_decode($ret); // 결과배열
		return $retArr;	//  단문/장문/MMS
	}
}

//난이도 이름
if ( ! function_exists('fncDiffcult')){
	function fncDiffcult($level){
		if($level=="1"){
			$level_name="최상";
		}elseif($level=="2"){
			$level_name="상";
		}elseif($level=="3"){
			$level_name="중상";
		}elseif($level=="4"){
			$level_name="중";
		}elseif($level=="5"){
			$level_name="중하";
		}elseif($level=="6"){
			$level_name="하";
		}else{
			$level_name="";
		}

		return $level_name;
	}
}

//SMS 발송잔여건수 알리고 API
if ( ! function_exists('fncAligoCount')){
	function fncAligoCount() {
		/**************** 전송가능 건수확인 ******************/
		/* "result_code":결과코드,"message":결과문구, */
		/** SMS_CNT / LMS_CNT / MMS_CNT : 전송유형별 잔여건수 ***/
		/******************** 인증정보 ********************/
		$sms_url = "https://apis.aligo.in/remain/";		// 전송요청 URL
		$sms['user_id'] = ds_id;						// SMS 아이디
		$sms['key'] = ds_key;							// 인증키
		/******************** 인증정보 ********************/

		$host_info = explode("/", $sms_url);
		$port = $host_info[0] == 'https:' ? 443 : 80;

		$oCurl = curl_init();
		curl_setopt($oCurl, CURLOPT_PORT, $port);
		curl_setopt($oCurl, CURLOPT_URL, $sms_url);
		curl_setopt($oCurl, CURLOPT_POST, 1);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS, $sms);
		curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);

		// CURL 접속이 안되는 경우 CURL 옵션안내를 참조 하셔서 옵션을 출력하여 확인해 주세요.
		// http://phpdoc.me/manual/kr/function.curl-setopt.php
		$ret = curl_exec($oCurl);
		curl_close($oCurl);
		//echo $ret;
		$retArr = json_decode($ret); // 결과배열
		return $retArr;	//  단문/장문/MMS
	}
}

//따옴표 데이터 일괄 처리
if ( ! function_exists('fncChkStringArr')){
	function fncChkStringArr($Data) {
		if($Data){
			foreach($Data as $key=>$value){
				//echo $key . " : " . $value . "</br>";
				$value=fncChkString1($value);
				$Data[$key]=$value;
			}
			return $Data;
		}else{
			return "";
		}

	}
}
//따옴표 데이터 일괄 처리
if ( ! function_exists('fncChkString1')){
	Function fncChkString1($text) {
		$fncChkString = str_replace("'" , "''" , $text);
		return $fncChkString;
	}
}

//쌍따옴표 데이터 일괄 처리
if ( ! function_exists('fncChkDStringArr')){
	function fncChkDStringArr($Data) {
		if($Data){
			foreach($Data as $key=>$value){
				//echo $key . " : " . $value . "</br>";
				$value=fncChkStringD($value);
				$Data[$key]=$value;
			}
			return $Data;
		}else{
			return "";
		}

	}
}
//쌍따옴표 데이터 일괄 처리
if ( ! function_exists('fncChkStringD')){
	function fncChkStringD($text) {
		$fncChkStringD = str_replace('"','&#34;',$text);
		return $fncChkStringD;
	}
}

//API용 데이터 가공
if ( ! function_exists('fncPostJson')){
	function fncAPICeatepaper($data) {
		$Newdata["request"]["parm"]			= "createpaper";	
		$Newdata["data"]["TEACHER_ID"]		= $_SESSION["_TEACHER"]["p_id"];
		$Newdata["data"]["PARENT_CODE"]		= $data["PARENT_CODE"];
		$Newdata["data"]["REFERENCE_CODE"]	= $data["REFERENCE_CODE"];
		$Newdata["data"]["TYPE_CODE"]		= $data["TYPE_CODE"];
		$Newdata["data"]["REAL_NAME"]		= $data["REAL_NAME"];
		$Newdata["data"]["FILE_TYPE"]		= $data["FILE_TYPE"];
		$Newdata["data"]["FILE_ROUTE"]		= $data["FILE_ROUTE"];
		$Newdata["data"]["FILE_URL"]		= $data["FILE_URL"];
		$Newdata["data"]["OG_NUM"]			= $data["OG_NUM"];

		$Newdata["file"]["original"]		= $data["original"];
		$Newdata["file"]["copy"]			= $data["copy"];

		$_param = "";
		$_this = json_encode($Newdata,true) ;
		$result = fncPostJson($_this,$_param);

		return $result;
	}
}

//API 넘기기
if ( ! function_exists('fncPostJson')){
	function fncPostJson($post_data,$method){
		//echo "Json_inner=>".print_r($post_data)."//".$method;
		$_method = $method == "" ? "" : "/".$method ;
		$url = "http://note.duworkplace.com/api_20200225" ;
		$headers = array(
			"content-type: application/json"
		) ;

		$arr_post = array() ;

		$ch=curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		//header값 셋팅(없을시 삭제해도 무방함)
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//POST방식
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);

		//POST방식으로 넘길 데이터(JSON데이터)
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		$response = curl_exec($ch);
		echo "<br><br>response=><br>".$response."<br><br>";
		if(curl_error($ch)){
			$curl_data = null;
		}else{
			$curl_data = $response;
		}

		curl_close($ch) ;
		return $curl_data ;
	}

	//카피되는 파일명
	if ( ! function_exists('fncCopyURL')){
		function fncCopyURL($FILE_URL,$group,$f_type,$number) {
			//$f_type  images , hwp
			if(substr($FILE_URL,0,5)=="https"){
				$http_type="https://";
			}else{
				$http_type="http://";
			}
			$FILE_URL		= str_replace("https://","",$FILE_URL);
			$FILE_URL		= str_replace("http://","",$FILE_URL);
			$FILE_URL_arr	= explode("/",$FILE_URL);
			$REAL_NAME = $FILE_URL_arr[5];						//리얼네임 전체
			$REAL_EXT = substr(strrchr($REAL_NAME, '.'), 1);	//확장자

			$cnt=count(explode("-",$number));
			if($cnt=="2"){	//지문
				$COPY_NAME="지문".$number.".".$REAL_EXT;
			}else{			//문항
				$COPY_NAME=$number.".".$REAL_EXT;
			}

			$FILE_URL=$http_type.$FILE_URL_arr[0]."/".$FILE_URL_arr[1]."/".$FILE_URL_arr[2]."/".$group."/".$f_type."/".$COPY_NAME;

			return $FILE_URL;
		}
	}
}

//API 넘기기
if ( ! function_exists('fncWeek')){
	function fncWeek($date){

		$week = array("일요일" , "월요일"  , "화요일" , "수요일" , "목요일" , "금요일" ,"토요일") ;
		$weekday = $week[ date('w'  , strtotime($date)  ) ] ;
		return $weekday;
	}
}

if ( ! function_exists('fncWeek_s')){
	function fncWeek_s($date){

		$week = array("(일)" , "(월)"  , "(화)" , "(수)" , "(목)" , "(금)" ,"(토)") ;
		$weekday = $week[ date('w'  , strtotime($date)  ) ] ;
		return $weekday;
	}
}


if ( ! function_exists('replaceTxt')){
    function replaceTxt($date){

        $week = array("(일)" , "(월)"  , "(화)" , "(수)" , "(목)" , "(금)" ,"(토)") ;
        $weekday = $week[ date('w'  , strtotime($date)  ) ] ;
        return $weekday;
    }
}


if ( ! function_exists('fncNaverSurl')){
    //URL 줄이기(네이버 New API)
    function fncNaverSurl($_url) {

        $encText = urlencode($_url);
        $postvars = "url=".$encText;
        $url = "https://openapi.naver.com/v1/util/shorturl";
        $is_post = true;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, $is_post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
        $headers = array();
        $headers[] = "X-Naver-Client-Id: ".cstNaver_ClientID;
        $headers[] = "X-Naver-Client-Secret: ".cstNaver_Secret;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //echo "status_code:".$status_code."<br>";
        curl_close ($ch);
        if($status_code == 200) {
//            echo "<br>".$response."<br>";
            $json=json_decode($response,true);
            $url=$json["result"]["url"];

            return $url;
        } else {
//            echo "원본 url".$_url ;
//            echo "Error 내용:".$response;
            return "";
            //echo "Error 내용:".$response;
        }
    }

}




function cut_str($str, $len, $suffix="…"){
    $arr_str = preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    $str_len = count($arr_str);

    if ($str_len >= $len) {
        $slice_str = array_slice($arr_str, 0, $len);
        $str = join("", $slice_str);

        return $str . ($str_len > $len ? $suffix : '');
    } else {
        $str = join("", $arr_str);
        return $str;
    }
}


function json_validate($string){
    // decode the JSON data

	$_bool = true ;

    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid // No error has occurred
            break;
        case JSON_ERROR_DEPTH:
            $error = 'The maximum stack depth has been exceeded.';
			$_bool = false ;
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Invalid or malformed JSON.';
			$_bool = false ;
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Control character error, possibly incorrectly encoded.';
			$_bool = false ;
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
			$_bool = false ;
            break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
			$_bool = false ;
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
            $error = 'One or more recursive references in the value to be encoded.';
			$_bool = false ;
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
            $error = 'One or more NAN or INF values in the value to be encoded.';
			$_bool = false ;
            break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
            $error = 'A value of a type that cannot be encoded was given.';
			$_bool = false ;
            break;
        default:
            $error = 'Unknown JSON error occured.';
			$_bool = false ;
            break;
    }

    if ($error !== '') {
        // throw the Exception or exit // or whatever :)
        exit($error);
    }

    // everything is OK
    return $_bool ;
}

if ( ! function_exists('fncUseAddDate')){
	//시작일, 종료일 사용일수
	function fncUseAddDate($date_s, $date_e) {
		$datetime1 = date_create($date_s);
		$datetime2 = date_create($date_e);
		$interval = date_diff($datetime1, $datetime2);
		$year =  $interval->format('%y'); // 10년 10개월 11일
		$month =  $interval->format('%m'); // 10년 10개월 11일
		$day =  $interval->format('%d'); // 10년 10개월 11일

		$datetime ="";
		if($year>0){ $datetime .= $year."년 "; }
		$datetime .= $month."개월 ";
		$datetime .= $day."일";
		$data["view"] = $datetime;
		$data["day"] = $day;
		$data["month"] = $month;

		return $data;
	}
}


function convertToReadableSize($size){
    if($size == 0){
        return 0 ;
    }else{
        $base = log($size) / log(1024) ;
        $suffix = array("B", "KB", "MB", "GB", "TB");
        $f_base = floor($base);
        return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    }

}

function chkZero($score){

    if(is_numeric($score)){
        if(strpos($score, ".") !== false) {
            $_score = explode(".",$score) ;
            if($_score[1] === "00"){
                $_s = $_score[0] ;
            }else{
                $_s = $score ;
            }
        }else{
            $_s = $score ;
        }

    }else{
        $_s = "-" ;
    }
    return $_s ;
}


//메일발송
Function fncSendMailHTML() {
	if (!func_get_args()){
		$msg = "오류메세지 : 메일보내기가 실패되었습니다. 인자가 부족합니다.";
		fncMsg($msg);
	}

	$mail_to = func_get_arg(0) ;
	$mail_subject = func_get_arg(1);
	$mail_contents = func_get_arg(2);
	$mail_femail = func_get_arg(3);
	$mail_fname = func_get_arg(4);

	$header  = "Return-Path: ".$mail_femail."\n";
	$header .= "From: =?UTF-8?B?".base64_encode($mail_fname)."?= <".$mail_femail.">\n";
	$header .= "MIME-Version: 1.0\n";
	$header .= "X-Priority: 3\n";
	$header .= "X-MSMail-Priority: Normal\n";
	$header .= "X-Mailer: FormMailer\n";
	$header .= "Content-Transfer-Encoding: base64\n";
	$header .= "Content-Type: text/html;\n \tcharset=utf-8\n";

	$mail_subject  = "=?UTF-8?B?".base64_encode($mail_subject)."?=\n";
	$mail_contents = base64_encode($mail_contents);

	$result = @mail ($mail_to , $mail_subject , $mail_contents , $header );

	return $result;

}

 //랜덤함수
 function get_random_string($type = '', $len = 10) {
    $lowercase = 'abcdefghijklmnopqrstuvwxyz';
    $uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $numeric = '0123456789';
    $special = '`~!@#$%^&*()-_=+\\|[{]};:\'",<.>/?';
    $key = '';
    $token = '';
    if ($type == '') {
        $key = $lowercase.$uppercase.$numeric;
    } else {
        if (strpos($type,'09') > -1) $key .= $numeric;
        if (strpos($type,'az') > -1) $key .= $lowercase;
        if (strpos($type,'AZ') > -1) $key .= $uppercase;
        if (strpos($type,'$') > -1) $key .= $special;
    }
    for ($i = 0; $i < $len; $i++) {
        $token .= $key[mt_rand(0, strlen($key) - 1)];
    }
    return $token;

	//사용예
	/*
	echo '기본 : ' . get_random_string() . '<br />';
	echo '숫자만 : ' . get_random_string('09') . '<br />';
	echo '숫자만 30글자 : ' . get_random_string('09', 30) . '<br />';
	echo '소문자만 : ' . get_random_string('az') . '<br />';
	echo '대문자만 : ' . get_random_string('AZ') . '<br />';
	echo '소문자+대문자 : ' . get_random_string('azAZ') . '<br />';
	echo '소문자+숫자 : ' . get_random_string('az09') . '<br />';
	echo '대문자+숫자 : ' . get_random_string('AZ09') . '<br />';
	echo '소문자+대문자+숫자 : ' . get_random_string('azAZ09') . '<br />';
	echo '특수문자만 : ' . get_random_string('$') . '<br />';
	echo '숫자+특수문자 : ' . get_random_string('09$') . '<br />';
	echo '소문자+특수문자 : ' . get_random_string('az$') . '<br />';
	echo '대문자+특수문자 : ' . get_random_string('AZ$') . '<br />';
	echo '소문자+대문자+특수문자 : ' . get_random_string('azAZ$') . '<br />';
	echo '소문자+대문자+숫자+특수문자 : ' . get_random_string('azAZ09$') . '<br />';
	*/
}

//AES암호화
function AES_ENCRYPT($val,$ky) { 
	$key = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; 
	for($a=0;$a<strlen($ky);$a++) $key[$a%16]=chr(ord($key[$a%16]) ^ ord($ky[$a])); 
	$mode = MCRYPT_MODE_ECB; 
	$enc = MCRYPT_RIJNDAEL_128; 
	$val = str_pad($val, (16*(floor(strlen($val) / 16)+(strlen($val) % 16==0?2:1))), chr(16-(strlen($val) % 16))); 
	return @mcrypt_encrypt($enc, $key, $val, $mode, @mcrypt_create_iv( @mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM)); 
}

//AES 복호화
function AES_DECRYPT($val,$ky) { 
	$key="\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; 
	for($a=0;$a<strlen($ky);$a++) $key[$a%16]=chr(ord($key[$a%16]) ^ ord($ky[$a])); 
	$mode = MCRYPT_MODE_ECB; 
	$enc = MCRYPT_RIJNDAEL_128; 
	$dec = @mcrypt_decrypt($enc, $key, $val, $mode, @mcrypt_create_iv( @mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM ) ); 
	return rtrim($dec,(( ord(substr($dec,strlen($dec)-1,1))>=0 and ord(substr($dec, strlen($dec)-1,1))<=16)? chr(ord( substr($dec,strlen($dec)-1,1))):null)); 
}



function fncAligoSMS_New($recipients,$message) {
    global  $ps_reservedte, $msg_id;	//아이디, 키, 발송자 등록번호, 예약시간, 발송코드(업체에서 제공)

    $sms_url = "https://apis.aligo.in/send/"; // 전송요청 URL
    $sms['user_id']	 = $_SESSION['site_info']['sms']['id']; // SMS 아이디
    $sms['key']		 = $_SESSION['site_info']['sms']['key'];//인증키
    $sms['sender']	 = $_SESSION['site_info']['sms']['phone'];			// 발신번호

    $sms['msg'] = stripslashes($message);
    $sms['receiver'] = $recipients;
    if($ps_reservedte<>""){
        $send_date=str_replace("-","",substr($ps_reservedte,0,10));
        $send_time=str_replace(":","",substr($ps_reservedte,11,5));

        $sms['rdate'] = $send_date;	//예약일자 - 20161004 : 2016-10-04일기준
        $sms['rtime'] = $send_time;	//예약시간 - 1930 : 오후 7시30분

    }


    //테스트 발송유무
    $sms['testmode_yn'] = '';			// Y 인경우 실제문자 전송X , 자동취소(환불) 처리

    $host_info = explode("/", $sms_url);
    $port = $host_info[0] == 'https:' ? 443 : 80;



//    $command = "curl ";
//    foreach ($sms as $key => &$val){
//        $command .= "-F '$key=$val' ";
//    }
//    $command .= "$sms_url -s > /dev/null 2>&1 &";
//    passthru($command);



    $oCurl = curl_init();
    curl_setopt($oCurl, CURLOPT_PORT, $port);
    curl_setopt($oCurl, CURLOPT_URL, $sms_url);
    curl_setopt($oCurl, CURLOPT_POST, 1);
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($oCurl, CURLOPT_POSTFIELDS, $sms);
    curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
    $ret = curl_exec($oCurl);
    curl_close($oCurl);

    //echo $ret;
    //exit;
    $retArr = json_decode($ret); // 결과배열
    $result_code = $retArr->{"result_code"};





//    echo "result_code->".$retArr->{"result_code"}."<br>";
//    echo "message->".$retArr->{"message"}."<br>";
//    echo "msg_id->".$retArr->{"msg_id"}."<br>";
//    echo "msg_type->".$retArr->{"msg_type"}."<br>";


    if($result_code=='1'){	//테스트 하면서 확인요망
        $msg_id = $retArr->{"msg_id"};

        $result["result_code"]="Y";
        $result["msg_id"]=$msg_id;

        $result["alram"]="Y";
        $result["wdate"] = date("Y-m-d H:i:s");

    }else{
        $result["result_code"]="N";
        $result["msg_id"]=$msg_id;

        $result["alram"]="N";
        $result["wdate"] = "1900-01-01 00:00:00";
    }

    return $result;

}



function syncSms($_param){
    $_data = base64_encode($_param) ;
    $command = "php -f ".$_SERVER['DOCUMENT_ROOT']."/index.php academy/reservation/set/menu/sms/{$_data}" ;
    $command .= " > /dev/null 2>&1 &";

    passthru($command,$status) ;

}


//데이터 찍어보기 bjsc3
function dataView($data) {

	echo "<div ID='ID_xbtn_open' style='z-index:99;position:absolute;margin-left:calc(50% - 400px);background-color:#FFFFFF;width:20;height:20px;font-size:20px;top:30px;cursor:pointer;text-align:center;' onclick=\"document.getElementById('ID_dataview').style.display='none';document.getElementById('ID_xbtn_open').style.display='none';document.getElementById('ID_xbtn_close').style.display='';\">X</div>";

	echo "<div ID='ID_xbtn_close' style='display:none;z-index:99;position:absolute;margin-left:calc(50% - 400px);background-color:#FFFFFF;width:20;height:20px;font-size:20px;top:30px;cursor:pointer;text-align:center;' onclick=\"document.getElementById('ID_dataview').style.display='';document.getElementById('ID_xbtn_open').style.display='';document.getElementById('ID_xbtn_close').style.display='none';\">X</div>";

	echo "<div id='ID_dataview' style='z-index:99;overflow-y:auto;position:absolute;background-color:#f1f1f1;border:solid 1px #6d6b78;width:800px;height:300px;margin-left:calc(50% - 400px);top:50px;padding:10px;'>";
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	echo "</div>";
}


// 페이징 NEW
function fncpageview($href, $intblockpage, $blockpage, $gp, $totalcnt, $ps, $pagecount, $param) {
	$Html="
	<!-- paging -->
	<div class=\"tbl_paging\" style=\"margin-top:20px;\">
		<!-- 페이징-->
		<div class=\"paging_inner\">
			<ul class=\"paging\">		
	";

	If ($totalcnt > 0) {

		If ($gp == 1) {
			$Html .= "
				<li>
				<a onclick=\"alert('페이지의 처음 입니다.');\" aria-label=\"Previous\"><i class=\"fa fa-angle-left\"></i></a>
				</li>
			";
		} Else {
			$Html .= "
				<li>
				<a href=\"".$href."?gp=".($gp-1)."&ps=".$ps.$param."\" aria-label=\"Previous\"><i class=\"fa fa-angle-left\"></i></a>
				</li>
			";
		}

		$j = 1;
		while (!(($blockpage > $pagecount) || ($j > $intblockpage))) {
			If ($blockpage == (int)$gp) {
				$Html .= "
					<li class=\"active\"><a href=\"#\">".$blockpage."</a></li>
				";
			} Else {
				$Html .= "
					<li><a href=\"".$href."?gp=".$blockpage."&ps=".$ps.$param."\">".$blockpage."</a></li>
				";
			}

			$blockpage = $blockpage+1;
			$j = $j + 1;
		}

		If ((int)$gp < $pagecount) {
			$Html .= "
			<li><a href=\"".$href."?gp=".($gp+1)."&ps=".$ps.$param."\"  aria-label=\"Next\"><i class=\"fa fa-angle-right\"></i></a></li>
			";
		} Else {
			$Html .= "
			<li><a onclick=\"alert('페이지 끝 입니다.')\" aria-label=\"Next\"><i class=\"fa fa-angle-right\"></i></a></li>
			";
		}
	}

	$Html .= "
			</ul>
		</div>
		<!-- //페이징-->
	</div>
	<!-- //paging -->
	";

	return $Html;

}

// 페이징 NEW
function fncpageview_new($href, $intblockpage, $blockpage, $gp, $totalcnt, $ps, $pagecount, $action) {
	$Html="
	<!-- paging -->
	<div class=\"tbl_paging\" style=\"margin-top:20px;\">
		<!-- 페이징-->
		<div class=\"paging_inner\">
			<ul class=\"paging\">		
	";

	If ($totalcnt > 0) {

		If ($gp == 1) {
			$Html .= "
				<li>
				<a onclick=\"alert('페이지의 처음 입니다.');\" aria-label=\"Previous\"><i class=\"fa fa-angle-left\"></i></a>
				</li>
			";
		} Else {
			$Html .= "
				<li>
				<a onclick=\"_jsPaging('".$ps."','".($gp-1)."','".$action."')\" aria-label=\"Previous\"><i class=\"fa fa-angle-left\"></i></a>
				</li>
			";
		}

		$j = 1;
		while (!(($blockpage > $pagecount) || ($j > $intblockpage))) {
			If ($blockpage == (int)$gp) {
				$Html .= "
					<li class=\"active\"><a href=\"#\">".$blockpage."</a></li>
				";
			} Else {
				$Html .= "
					<li><a onclick=\"_jsPaging('".$ps."','".$blockpage."','".$action."')\">".$blockpage."</a></li>
				";
			}

			$blockpage = $blockpage+1;
			$j = $j + 1;
		}

		If ((int)$gp < $pagecount) {
			$Html .= "
			<li><a onclick=\"_jsPaging('".$ps."','".($gp+1)."','".$action."')\" aria-label=\"Next\"><i class=\"fa fa-angle-right\"></i></a></li>
			";
		} Else {
			$Html .= "
			<li><a onclick=\"alert('페이지 끝 입니다.')\" aria-label=\"Next\"><i class=\"fa fa-angle-right\"></i></a></li>
			";
		}
	}

	$Html .= "
			</ul>
		</div>
		<!-- //페이징-->
	</div>
	<!-- //paging -->
	";

	return $Html;

}

function fncsetpagesize($intblockpage, $blockpage, $gp, $totalcnt, $ps, $pagecount) {

	$intblockpage = 10;
	$blockpage = (int)(($gp - 1)/$intblockpage) * $intblockpage+1;
	If ((preg_match("/[0-9]/",$totalcnt)) && ($totalcnt > 0)) {
		$pagecount = (int)(($totalcnt - 1)/$ps)+1;
	} Else {
		$pagecount = 0;
	}
	//$pagecount = 1;

	$_data["intblockpage"]=$intblockpage;
	$_data["blockpage"]=$blockpage;
	$_data["gp"]=$gp;
	$_data["totalcnt"]=$totalcnt;
	$_data["ps"]=$ps;
	$_data["pagecount"]=$pagecount;

	return $_data;
}


// 자바스크립의 ALERT 후 CONFIRM
if ( ! function_exists('fncCfm')){
	function fncCfm(){
		global $msg , $msg2, $msg3 ;

		if (!func_get_args()){
			$msg = '오류메세지 : 메세지가 없습니다.\\n\\n관리자에게 문의하시기 바랍니다.';
		}
		$msg = func_get_arg(0);

		if (func_num_args() > 1){
			$msg2 = func_get_arg(1);
		}

		if (func_num_args() > 2){
			$msg3 = func_get_arg(2);
		}

		echo "<script type='text/JavaScript'>";
		if ($msg!=""){	echo "if(confirm('".$msg."')){";}
		if ($msg2!=""){	echo $msg2;	}
		if ($msg!=""){	echo "}else{";		}
		if ($msg3!=""){	echo $msg3;	}
		if ($msg!=""){	echo "}";		}
		echo "</script><noscript></noscript></body></html>";
		exit;
	}
}


function fncMsg2($msg,$url,$target) {
	if($target<>""){ $target=$target."."; }
	echo "<script language='javascript'>";
	if($msg!=""){	echo "alert('".$msg."');";	}
	if($url!=""){	echo $target."location.href='".$url."';";	}
	echo "</script>";
}



function scoreView($score){
    $_score = $score * 100 ;
    return $_score / 100 ;
}


// 하이픈 추가
function add_hyphen($tel){
    $tel = preg_replace("/[^0-9]*/s", "", $tel) ;//숫자이외 제거
    if (substr($tel, 0, 2) == '02') {
        return preg_replace("/([0-9]{2})([0-9]{3,4})([0-9]{4})$/", "\\1-\\2-\\3", $tel);
    } else if (substr($tel, 0, 2) == '8' && substr($tel, 0, 2) == '15' || substr($tel, 0, 2) == '16' || substr($tel, 0, 2) == '18') {
        return preg_replace("/([0-9]{4})([0-9]{4})$/", "\\1-\\2", tel);
        //지능망 번호이면
    }else{
        return preg_replace("/([0-9]{3})([0-9]{3,4})([0-9]{4})$/", "\\1-\\2-\\3", $tel);
        //핸드폰번호만 이용한다면 이것만잇어도됨
    }
}

//캡챠
function generateCode($characters) {
	/* list all possible characters, similar looking characters and vowels have been removed */
	$possible = '23456789bcdfghjkmnpqrstvwxyz';
	$code = '';
	$i = 0;
	while ($i < $characters) { 
		$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
		$i++;
	}
	return $code;
}

//캡챠
function CaptchaSecurityImages($width='120',$height='40',$characters='6') {

	$font = '/home/toptier/public_html/captcha/monofont.ttf';

	$code = generateCode($characters);
	/* font size will be 75% of the image height */
	$font_size = $height * 0.75;
	$image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
	/* set the colours */
	$background_color = imagecolorallocate($image, 255, 255, 255);
	$text_color = imagecolorallocate($image, 20, 40, 100);
	$noise_color = imagecolorallocate($image, 100, 120, 180);
	/* generate random dots in background */
	for( $i=0; $i<($width*$height)/3; $i++ ) {
		imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
	}
	/* generate random lines in background */
	for( $i=0; $i<($width*$height)/150; $i++ ) {
		imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
	}
	/* create textbox and add text */
	$textbox = imagettfbbox($font_size, 0, $font, $code) or die('Error in imagettfbbox function');
	$x = ($width - $textbox[4])/2;
	$y = ($height - $textbox[5])/2;
	imagettftext($image, $font_size, 0, $x, $y, $text_color, $font , $code) or die('Error in imagettftext function');
	/* output captcha image to browser */
	//header('Content-Type: image/jpeg');
	//imagejpeg($image);
	//imagedestroy($image);
	$_SESSION['security_code'] = $code;


	return $image;

}

//마스킹
function letterMasking($_type, $_data){
	$_data = str_replace('-','',$_data);
	$strlen = mb_strlen($_data, 'utf-8');
	$maskingValue = "";

	$useHyphen = "-";

	if($_type == 'N'){
		switch($strlen){
			case 2:
				$maskingValue = mb_strcut($_data, 0, 3, "UTF-8").'*';
				break;
			case 3:
				$maskingValue = mb_strcut($_data, 0, 3, "UTF-8").'*'.mb_strcut($_data, 8, 11, "UTF-8");
				break;
			case 4:
				$maskingValue = mb_strcut($_data, 0, 3, "UTF-8").'**'.mb_strcut($_data, 12, 15, "UTF-8");
				break;
			default:
				$maskingValue = mb_strcut($_data, 0, 3, "UTF-8").'**'.mb_strcut($_data, 12, 15, "UTF-8");
				break;
		}
	}else if($_type == 'P'){
		switch($strlen){
			case 10:
				$maskingValue = mb_substr($_data, 0, 3)."{$useHyphen}***{$useHyphen}".mb_substr($_data, 6, 4);
				break;
			case 11:
				$maskingValue = mb_substr($_data, 0, 3)."{$useHyphen}****{$useHyphen}".mb_substr($_data, 7, 4);
				break;
			default:
				trigger_error('Not a known format parametter in function', E_USER_NOTICE);
				break;
		}
	}else{
		trigger_error('Masking Function Parameter Error', E_USER_NOTICE);
	}

	return $maskingValue;
}

//OMR 파일삭제
function OmrFileDelete($fname){
	$is_file_exist = file_exists($_SERVER['DOCUMENT_ROOT']."/".direct_omr."/".$fname);
	//파일 있으면
	if($is_file_exist){
		//삭제 안되면
		if( !unlink($_SERVER['DOCUMENT_ROOT']."/".direct_omr."/".$fname) ) {
			$result = false;
		}else{
			$result = true;
		}

	}else{
		$result = true;
	}

	return $result;

}

// 커리큘럼 PDF 파일삭제
function curriculumFileDelete($fname){
	$is_file_exist = file_exists(curriculum_upload."/".$fname);
	//파일 있으면
	if($is_file_exist){
		//삭제 안되면
		if( !unlink(curriculum_upload.$fname) ) {
			$result = false;
		}else{
			$result = true;
		}

	}else{
		$result = true;
	}

	return $result;

}

//커리큘럼 pdf 파일명 변경
function curriculumFileRename($fname_old, $fname_new) {
	$result = rename(curriculum_upload.$fname_old,curriculum_upload.$fname_new);
	return $result;
}


//pre 보기
function fncpre($data) {
	echo "<br><br><br><pre>";
	print_r($data);
	echo "</pre><br><br><br>";
}


function GenerateString($length){
    $characters  = "0123456789";
    $characters .= "abcdefghijklmnopqrstuvwxyz";
    $characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//    $characters .= "_";

    $string_generated = "";

    $nmr_loops = $length;
    while ($nmr_loops--)
    {
        $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string_generated;
}

function alert_js($act,$Message=Null,$url){
    $Message = str_replace("'", "\"", trim($Message));

    $act_obj = explode("_",$act);

    echo "<script language='javascript'>";

    for($_i=0;$_i<sizeof($act_obj);$_i++){
        switch($act_obj[$_i]){
            case("alert"):			echo "alert('".$Message."');";			 	        break;
            case("move"):			echo "location.href = '".$url."';";	                break;
            case("back"):			echo "history.back();";							    break;
            case("reload"):			echo "location.reload();";					        break;
            case("opener"):			echo "opener.";										break;
            case("parent"):			echo "parent.";										break;
            case("selfclose"):	    echo "self.close();";								break;
            case("selfclose"):	    echo "self.close();";								break;
            case("fnc"):			echo $url;											break;

        }
    }

    echo "</script>";

}
