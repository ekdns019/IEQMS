<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SADM_Controller extends MY_Controller {
    protected $_sessionNm  ;
    protected $infoObj     ;
    protected $searchObj   ;
    protected $apiObj      ;
    protected $PATH        ;
    protected $firstPage   ;
    public $admInfo        ;

    public function __construct(){
        parent::__construct() ;
        $this->_sessionNm = "_ADMIN"                          ;
        $this->infoObj    = new stdClass()                    ;
        $this->searchObj  = new stdClass()                    ;
        $this->apiObj     = new stdClass()                    ;
        $this->PATH       = 'inc/'.$this->__SEG[1].'/'        ;
        $this->firstPage  = $this->getFirstPage()             ;

        // user정보 변수
        if($this->USER_AUTH($this->_sessionNm)){
            $this->admInfo = $this->session->userdata[$this->_sessionNm]['user'] ;

        }
    }


    private function getFirstPage(){
        $_user = $this->_sessionNm ;
        if($this->USER_AUTH($_user)){
            $_page = "sadm/main/main" ;
        }else{
            $_page = "sadm/login" ;
        }
        return $_page ;
    }


    // sadm 페이지
    public function loadPage($page,$_data,$include=array(),$use=null){
        /*
         * $include[0] = top
         * $include[1] = header
         * $include[2] = footer
         * $include[3] = bottom
         */

        // 메뉴 사용여부
        $_USE = explode(",",$use) ;

        // 공통 페이지 데이터
        $data = array(
            "TITLE" => "문제은행관리 시스템 총관리자",
            "MENU"   => $this->getMenu($this->_sessionNm) ,
        ) ;

        if($_USE[0] == "Y"){
            $this->load->load_view($this->PATH , $include[0], $data) ;
        }
        if($_USE[1] == "Y"){
            $this->load->load_view($this->PATH , $include[1], $data) ;
        }
        $this->load->view($page, $_data);
        if($_USE[2] == "Y"){
            $this->load->load_view($this->PATH , $include[2], $data) ;
        }
        if($_USE[3] == "Y"){
            $this->load->load_view($this->PATH , $include[3], $data) ;
        }
    }

    /*
     * Type Of boolean
     * $_bool = 사용자의 권한과 값이 같아야한다.
     */
    public function setDefault($_bool){
        if(!($this->USER_AUTH($this->_sessionNm) === $_bool)){
            redirect(base_url()."$this->firstPage") ;
        }
    }

}
