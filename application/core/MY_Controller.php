<?php
/**
 * Created by PhpStorm.
 * User: daun
 * Date: 2019-03-12
 * Time: 13:29
 */
require APPPATH.Du.'/Trait_auth.php'       ;
defined('BASEPATH') OR exit('No direct script access allowed') ;

class MY_Controller extends CI_Controller {
    use Trait_auth ;
    public $__SEG ;
    public function __construct(){
        parent::__construct()                       ;
        $this->__SEG = $this->uri->segment_array()  ;
        $this->load->database()                     ;
        $this->load->helper(array(
            'common',
            'form',
            'url'
        ));
    }
}
