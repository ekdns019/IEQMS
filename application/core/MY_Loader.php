<?php
/**
 * Created by PhpStorm.
 * User: daun
 * Date: 2019-03-12
 * Time: 13:48
 */
defined('BASEPATH') OR exit('No direct script access allowed') ;
class MY_Loader extends CI_Loader {
    public function __construct(){
        parent::__construct();
    }

    public function load_view($folder=NULL,$view, $vars = array(), $return = FALSE) {

        $this->_ci_root_paths = array_merge($this->_ci_root_paths, array(ROOTPATH . $folder . '/' => TRUE));

        if (method_exists($this, '_ci_object_to_array')){
            return $this->_ci_root_load(array(
                '_ci_view' => $view,
                '_ci_vars' => $this->_ci_object_to_array($vars),
                '_ci_return' => $return
            ));
        } else {

            return $this->_ci_root_load(array(
                '_ci_view' => $view,
                '_ci_vars' => $this->_ci_prepare_view_vars($vars),
                '_ci_return' => $return
            ));

        }
    }
}
