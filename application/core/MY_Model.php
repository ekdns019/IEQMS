<?php
/**
 * Created by PhpStorm.
 * User: daun
 * Date: 2019-03-12
 * Time: 12:46
 */
defined('BASEPATH') OR exit('No direct script access allowed') ;
class MY_Model extends CI_Model {
    use Trait_auth ;
    public function __construct(){
        parent::__construct()               ;
        $this->common_db = "PSEDU_COMMON"   ; // 공통디비
        $this->main_db   = "IEQMS_ADM"      ; // 문제은행 관리디비
        $this->sub_db    = "IEQMS_USR"      ; // 문제은행 유저디비
        $this->oauth_db  = "OAUTH"          ; // oauth 디비
        $this->m_db = $this->load->database("default",true)    ;

        /* 학원 세션 있을때 학원디비 연결 */
        if(!$this->USER_AUTH("_ACADEMY")){
            $this->academy_db = $this->session->userdata['_ACADEMY']['academy_id'] ;
            $config['hostname'] = 'localhost';
            $config['username'] = 'psedu';
            $config['password'] = 'psedu1q2w3e?#';
            $config['database'] = $this->academy_db ;
            $config['dbdriver'] = 'mysqli';
            $config['dbprefix'] = '';
            $config['pconnect'] = FALSE;
            $config['db_debug'] = FALSE;
            $config['cache_on'] = FALSE;
            $config['cachedir'] = '';
            $config['char_set'] = 'utf8';
            $config['dbcollat'] = 'utf8_general_ci';
            $this->a_db = $this->load->database($config, true) ;
        }
    }

    /**
     * @param $data = 데이터
     * @return mixed
     * 암호화할 데이터 인코딩처리
     */
    public function AES_ENCRYPT($data,$type=null){

        if(empty($type)){
            $_code = BANK ;
        }
        $_data = "HEX(AES_ENCRYPT('".$data."', SHA2('".$_code."',512)))" ;
        return $_data ;
    }

    /**
     * @param $field = 필드
     * @return string
     * 암호화필드 디코딩 처리
     */
    public function AES_DECRYPT($field,$name=null,$type=null){

        if(empty($type)){
            $_code = BANK ;
        }

        if($name!=null){
            $_data = "CONVERT(AES_DECRYPT(UNHEX(".$field."), SHA2('".$_code."',512)) USING utf8) as ".$name ;
        }else{
            $_data = "CONVERT(AES_DECRYPT(UNHEX(".$field."), SHA2('".$_code."',512)) USING utf8)" ;
        }
        return $_data ;
    }

}
